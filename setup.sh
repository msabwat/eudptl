# https://blog.guillaume-gomez.fr/Linux-kernel/1/1
# https://gist.github.com/chrisdone/02e165a0004be33734ac2334f215380e
exit 1 #don't run :)
CURRENT=`pwd`
# download busybox
git clone git://git.busybox.net/busybox || true
# download linux
git clone --depth=1 git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git || true

# Step1:
# cross-compile (same arch !!)
cd busybox
make defconfig
# I want it static:
sed -i '/CONFIG_STATIC /s/.*/CONFIG_STATIC=y/' .config 
make && make install
# important:
chmod u+s ./_install/bin/busybox

cd $CURRENT

# Step2:
# minimal rootfs
dd if=/dev/zero of=disk.img bs=1M count=32
mkfs.ext4 disk.img -L root
mkdir -p mnt
# !! sudo modprobe loop!!
mount disk.img mnt
cd mnt
mkdir -p bin sbin etc proc sys usr/bin usr/sbin
cp -r busybox/_install/* .
echo "#!/bin/sh

mount -t proc none /proc
mount -t sysfs none /sys
mount -t debugfs none /sys/kernel/debug
echo /sbin/mdev > /proc/sys/kernel/hotplug
#mdev -sdfv
mdev -sd
exec /bin/sh

cat <<!


Boot took $(cut -d' ' -f1 /proc/uptime) seconds


  _____ _____ _  __          _____ _    _ _    ___   __
 |  __ \_   _| |/ /    /\   / ____| |  | | |  | \ \ / /
 | |__) || | | ' /    /  \ | |    | |__| | |  | |\ V / 
 |  ___/ | | |  <    / /\ \| |    |  __  | |  | | > <  
 | |    _| |_| . \  / ____ \ |____| |  | | |__| |/ . \ 
 |_|   |_____|_|\_\/_/    \_\_____|_|  |_|\____//_/ \_\
                                                       
                                                       

Welcome to pikachux


!
exec /bin/sh" > init
chmod +x init
umount mnt
#step 3: linux
cd linux
make defconfig
#sed -i '/CONFIG_MODVERSIONS /s/.*/CONFIG_MODVERSIONS=y/' .config 
#interesting option to write more portable modules
make

cd $CURRENT

qemu-system-x86_64 -kernel linux/arch/x86/boot/bzImage -hda ./disk.img -append "earlyprintk=serial,ttyS0 console=ttyS0 init=/init root=/dev/sda rw" -enable-kvm -m 2048 -nographic -monitor unix:qemu-monitor-socket,server,nowait -usb

# pour avoir le qemu monitor:
# socat -,echo=0,icanon=0 unix-connect:qemu-monitor-socket
## device_add usb-kbd,id=pikaboard
## device_del pikaboard


## instructions for linux-next
## git remote add linux-next git://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git
## git fetch --tags linux-next
## git checkout master
## git checkout -b next-42 next-20220609


## Style check:
./linux/checkpatch.pl fichier.c -no-tree -file --strict
# or
find . -type f -name "*.c" -exec grep -i 'MODULE_VERSION' {} \; -exec ../linux/scripts/checkpatch.pl -no-tree -file --strict {} +


## debugging with qemu : (assuming the config has the right options)
## + (add -sS to qemu), target remote localhost:1234
https://qemu-project.gitlab.io/qemu/system/gdb.html

## debugging a kernel module ??
