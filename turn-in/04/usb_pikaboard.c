// SPDX-License-Identifier: GPL-2.0-only
/*
 *
 *
 *  Copyright (C) 2022  Mehdi Sabwat
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/usb.h>
#include <linux/hid.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mehdi Sabwat <mehdisabwat@gmail.com>");
MODULE_DESCRIPTION("Handle usb keyboard hotplug.");
MODULE_VERSION("0.1");

/*
 * 1. need CONFIG_UEVENT_HELPER=y
 * to allow mdev to configure hotplugging
 * 2. need to add the MODALIAS setup in mdev.conf && alias to modprobe.conf
 * 3. launch mdev as a daemon in /init to receive the right uevent
 */

static const struct usb_device_id usb_table[] = {
	{ USB_INTERFACE_INFO(USB_INTERFACE_CLASS_HID,
			     USB_INTERFACE_SUBCLASS_BOOT,
			     USB_INTERFACE_PROTOCOL_KEYBOARD) },
	{}
};
MODULE_DEVICE_TABLE(usb, usb_table);

static int __init pika_init(void)
{
	pr_info("Hello from pikaboard module\n");
	return 0;
}

static void __exit pika_exit(void)
{
	pr_info("Bye from pikaboard module\n");
}

module_init(pika_init);
module_exit(pika_exit);
