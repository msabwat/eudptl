c[?7l[2J[0mSeaBIOS (version ArchLinux 1.15.0-1)


iPXE (http://ipxe.org) 00:03.0 CA00 PCI2.10 PnP PMM+7FF91300+7FEF1300 CA00
Press Ctrl-B to configure iPXE (PCI 00:03.0)...                                                                               


Booting from ROM..c[?7l[2JNo EFI environment detected.
early console in extract_kernel
input_data: 0x0000000002b7a40d
input_len: 0x0000000000a53d18
output: 0x0000000001000000
output_len: 0x0000000002587ac8
kernel_total_size: 0x0000000002230000
needed_size: 0x0000000002600000
trampoline_32bit: 0x000000000009d000
Physical KASLR using RDTSC...
Virtual KASLR using RDTSC...

Decompressing Linux... Parsing ELF... Performing relocations... done.
Booting the kernel.
[    0.000000] Linux version 5.19.0-rc1-next-20220609 (b1ue@b1uesea) (gcc (GCC) 11.2.0, GNU ld (GNU Binutils) 2.38) #4 SMP PREEMPT_DYNAMIC Thu Sep 8 14:08:51 CEST 2022
[    0.000000] Command line: earlyprintk=serial,ttyS0 console=ttyS0 init=/init root=/dev/sda rw
[    0.000000] x86/fpu: x87 FPU will use FXSAVE
[    0.000000] signal: max sigframe size: 1440
[    0.000000] BIOS-provided physical RAM map:
[    0.000000] BIOS-e820: [mem 0x0000000000000000-0x000000000009fbff] usable
[    0.000000] BIOS-e820: [mem 0x000000000009fc00-0x000000000009ffff] reserved
[    0.000000] BIOS-e820: [mem 0x00000000000f0000-0x00000000000fffff] reserved
[    0.000000] BIOS-e820: [mem 0x0000000000100000-0x000000007ffdffff] usable
[    0.000000] BIOS-e820: [mem 0x000000007ffe0000-0x000000007fffffff] reserved
[    0.000000] BIOS-e820: [mem 0x00000000feffc000-0x00000000feffffff] reserved
[    0.000000] BIOS-e820: [mem 0x00000000fffc0000-0x00000000ffffffff] reserved
[    0.000000] printk: bootconsole [earlyser0] enabled
[    0.000000] NX (Execute Disable) protection: active
[    0.000000] SMBIOS 2.8 present.
[    0.000000] DMI: QEMU Standard PC (i440FX + PIIX, 1996), BIOS ArchLinux 1.15.0-1 04/01/2014
[    0.000000] Hypervisor detected: KVM
[    0.000000] kvm-clock: Using msrs 4b564d01 and 4b564d00
[    0.000002] kvm-clock: using sched offset of 476243604 cycles
[    0.000902] clocksource: kvm-clock: mask: 0xffffffffffffffff max_cycles: 0x1cd42e4dffb, max_idle_ns: 881590591483 ns
[    0.003710] tsc: Detected 2494.332 MHz processor
[    0.005186] last_pfn = 0x7ffe0 max_arch_pfn = 0x400000000
[    0.006128] x86/PAT: Configuration [0-7]: WB  WC  UC- UC  WB  WP  UC- WT  
Memory KASLR using RDTSC...
[    0.015984] found SMP MP-table at [mem 0x000f5b90-0x000f5b9f]
[    0.017368] ACPI: Early table checksum verification disabled
[    0.018385] ACPI: RSDP 0x00000000000F59D0 000014 (v00 BOCHS )
[    0.019346] ACPI: RSDT 0x000000007FFE1905 000034 (v01 BOCHS  BXPC     00000001 BXPC 00000001)
[    0.020763] ACPI: FACP 0x000000007FFE17B9 000074 (v01 BOCHS  BXPC     00000001 BXPC 00000001)
[    0.022184] ACPI: DSDT 0x000000007FFE0040 001779 (v01 BOCHS  BXPC     00000001 BXPC 00000001)
[    0.023591] ACPI: FACS 0x000000007FFE0000 000040
[    0.024356] ACPI: APIC 0x000000007FFE182D 000078 (v01 BOCHS  BXPC     00000001 BXPC 00000001)
[    0.025798] ACPI: HPET 0x000000007FFE18A5 000038 (v01 BOCHS  BXPC     00000001 BXPC 00000001)
[    0.027342] ACPI: WAET 0x000000007FFE18DD 000028 (v01 BOCHS  BXPC     00000001 BXPC 00000001)
[    0.028964] ACPI: Reserving FACP table memory at [mem 0x7ffe17b9-0x7ffe182c]
[    0.030121] ACPI: Reserving DSDT table memory at [mem 0x7ffe0040-0x7ffe17b8]
[    0.031309] ACPI: Reserving FACS table memory at [mem 0x7ffe0000-0x7ffe003f]
[    0.032460] ACPI: Reserving APIC table memory at [mem 0x7ffe182d-0x7ffe18a4]
[    0.033673] ACPI: Reserving HPET table memory at [mem 0x7ffe18a5-0x7ffe18dc]
[    0.034893] ACPI: Reserving WAET table memory at [mem 0x7ffe18dd-0x7ffe1904]
[    0.036381] No NUMA configuration found
[    0.037011] Faking a node at [mem 0x0000000000000000-0x000000007ffdffff]
[    0.038143] NODE_DATA(0) allocated [mem 0x7ffdc000-0x7ffdffff]
[    0.039135] Zone ranges:
[    0.039555]   DMA      [mem 0x0000000000001000-0x0000000000ffffff]
[    0.040634]   DMA32    [mem 0x0000000001000000-0x000000007ffdffff]
[    0.041775]   Normal   empty
[    0.042282] Movable zone start for each node
[    0.043032] Early memory node ranges
[    0.043679]   node   0: [mem 0x0000000000001000-0x000000000009efff]
[    0.045015]   node   0: [mem 0x0000000000100000-0x000000007ffdffff]
[    0.046118] Initmem setup node 0 [mem 0x0000000000001000-0x000000007ffdffff]
[    0.047756] On node 0, zone DMA: 1 pages in unavailable ranges
[    0.047788] On node 0, zone DMA: 97 pages in unavailable ranges
[    0.055525] On node 0, zone DMA32: 32 pages in unavailable ranges
[    0.057023] ACPI: PM-Timer IO Port: 0x608
[    0.058746] ACPI: LAPIC_NMI (acpi_id[0xff] dfl dfl lint[0x1])
[    0.059746] IOAPIC[0]: apic_id 0, version 17, address 0xfec00000, GSI 0-23
[    0.061040] ACPI: INT_SRC_OVR (bus 0 bus_irq 0 global_irq 2 dfl dfl)
[    0.062119] ACPI: INT_SRC_OVR (bus 0 bus_irq 5 global_irq 5 high level)
[    0.063198] ACPI: INT_SRC_OVR (bus 0 bus_irq 9 global_irq 9 high level)
[    0.064281] ACPI: INT_SRC_OVR (bus 0 bus_irq 10 global_irq 10 high level)
[    0.065400] ACPI: INT_SRC_OVR (bus 0 bus_irq 11 global_irq 11 high level)
[    0.066516] ACPI: Using ACPI (MADT) for SMP configuration information
[    0.067573] ACPI: HPET id: 0x8086a201 base: 0xfed00000
[    0.068445] smpboot: Allowing 1 CPUs, 0 hotplug CPUs
[    0.069325] PM: hibernation: Registered nosave memory: [mem 0x00000000-0x00000fff]
[    0.070573] PM: hibernation: Registered nosave memory: [mem 0x0009f000-0x0009ffff]
[    0.071823] PM: hibernation: Registered nosave memory: [mem 0x000a0000-0x000effff]
[    0.073062] PM: hibernation: Registered nosave memory: [mem 0x000f0000-0x000fffff]
[    0.074302] [mem 0x80000000-0xfeffbfff] available for PCI devices
[    0.075314] Booting paravirtualized kernel on KVM
[    0.076104] clocksource: refined-jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 1910969940391419 ns
[    0.083766] setup_percpu: NR_CPUS:64 nr_cpumask_bits:64 nr_cpu_ids:1 nr_node_ids:1
[    0.085458] percpu: Embedded 52 pages/cpu s176040 r8192 d28760 u2097152
[    0.086639] Fallback order for Node 0: 0 
[    0.087322] Built 1 zonelists, mobility grouping on.  Total pages: 515808
[    0.088479] Policy zone: DMA32
[    0.088992] Kernel command line: earlyprintk=serial,ttyS0 console=ttyS0 init=/init root=/dev/sda rw
[    0.090882] Dentry cache hash table entries: 262144 (order: 9, 2097152 bytes, linear)
[    0.092305] Inode-cache hash table entries: 131072 (order: 8, 1048576 bytes, linear)
[    0.093669] mem auto-init: stack:off, heap alloc:off, heap free:off
[    0.101544] Memory: 2024744K/2096632K available (16396K kernel code, 2657K rwdata, 5216K rodata, 1272K init, 1192K bss, 71628K reserved, 0K cma-reserved)
[    0.104052] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=1, Nodes=1
[    0.105165] Kernel/User page tables isolation: enabled
[    0.108390] Dynamic Preempt: voluntary
[    0.109308] rcu: Preemptible hierarchical RCU implementation.
[    0.110280] rcu: 	RCU event tracing is enabled.
[    0.111409] rcu: 	RCU restricting CPUs from NR_CPUS=64 to nr_cpu_ids=1.
[    0.112556] 	Trampoline variant of Tasks RCU enabled.
[    0.113430] rcu: RCU calculated value of scheduler-enlistment delay is 100 jiffies.
[    0.114770] rcu: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=1
[    0.117262] NR_IRQS: 4352, nr_irqs: 256, preallocated irqs: 16
[    0.118510] rcu: srcu_init: Setting srcu_struct sizes based on contention.
[    0.126002] Console: colour VGA+ 80x25
[    0.126666] printk: console [ttyS0] enabled
[    0.126666] printk: console [ttyS0] enabled
[    0.128244] printk: bootconsole [earlyser0] disabled
[    0.128244] printk: bootconsole [earlyser0] disabled
[    0.129883] ACPI: Core revision 20220331
[    0.130715] clocksource: hpet: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 19112604467 ns
[    0.132372] APIC: Switch to symmetric I/O mode setup
[    0.134746] ..TIMER: vector=0x30 apic1=0 pin1=2 apic2=-1 pin2=-1
[    0.135834] clocksource: tsc-early: mask: 0xffffffffffffffff max_cycles: 0x23f44f92df3, max_idle_ns: 440795329494 ns
[    0.137644] Calibrating delay loop (skipped) preset value.. 4988.66 BogoMIPS (lpj=2494332)
[    0.138637] pid_max: default: 32768 minimum: 301
[    0.139662] LSM: Security Framework initializing
[    0.140647] SELinux:  Initializing.
[    0.141651] Mount-cache hash table entries: 4096 (order: 3, 32768 bytes, linear)
[    0.142640] Mountpoint-cache hash table entries: 4096 (order: 3, 32768 bytes, linear)
Poking KASLR using RDTSC...
[    0.145348] Last level iTLB entries: 4KB 0, 2MB 0, 4MB 0
[    0.145639] Last level dTLB entries: 4KB 0, 2MB 0, 4MB 0, 1GB 0
[    0.146644] Spectre V1 : Mitigation: usercopy/swapgs barriers and __user pointer sanitization
[    0.147638] Spectre V2 : Mitigation: Retpolines
[    0.148636] Spectre V2 : Spectre v2 / SpectreRSB mitigation: Filling RSB on context switch
[    0.149636] Speculative Store Bypass: Vulnerable
[    0.150640] MDS: Vulnerable: Clear CPU buffers attempted, no microcode
[    0.165600] Freeing SMP alternatives memory: 44K
[    0.268635] smpboot: CPU0: Intel QEMU Virtual CPU version 2.5+ (family: 0xf, model: 0x6b, stepping: 0x1)
[    0.268810] cblist_init_generic: Setting adjustable number of callback queues.
[    0.269636] cblist_init_generic: Setting shift to 0 and lim to 1.
[    0.270658] Performance Events: unsupported Netburst CPU model 107 no PMU driver, software events only.
[    0.271994] rcu: Hierarchical SRCU implementation.
[    0.272009] printk: console [ttyS0] printing thread started
[    0.274659] smp: Bringing up secondary CPUs ...
[    0.274662] smp: Brought up 1 node, 1 CPU
[    0.274664] smpboot: Max logical packages: 1
[    0.274665] smpboot: Total of 1 processors activated (4988.66 BogoMIPS)
[    0.274903] devtmpfs: initialized
[    0.275160] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 1911260446275000 ns
[    0.275164] futex hash table entries: 256 (order: 2, 16384 bytes, linear)
[    0.275277] PM: RTC time: 12:10:49, date: 2022-09-08
[    0.275426] NET: Registered PF_NETLINK/PF_ROUTE protocol family
[    0.275491] audit: initializing netlink subsys (disabled)
[    0.275585] thermal_sys: Registered thermal governor 'step_wise'
[    0.275586] thermal_sys: Registered thermal governor 'user_space'
[    0.275590] cpuidle: using governor menu
[    0.275771] PCI: Using configuration type 1 for base access
[    0.282830] kprobes: kprobe jump-optimization is enabled. All kprobes are optimized if possible.
[    0.282868] audit: type=2000 audit(1662639050.225:1): state=initialized audit_enabled=0 res=1
[    0.415650] HugeTLB: can optimize 7 vmemmap pages for hugepages-2048kB
[    0.415653] HugeTLB registered 2.00 MiB page size, pre-allocated 0 pages
[    0.415794] ACPI: Added _OSI(Module Device)
[    0.415795] ACPI: Added _OSI(Processor Device)
[    0.415796] ACPI: Added _OSI(3.0 _SCP Extensions)
[    0.415796] ACPI: Added _OSI(Processor Aggregator Device)
[    0.415797] ACPI: Added _OSI(Linux-Dell-Video)
[    0.415798] ACPI: Added _OSI(Linux-Lenovo-NV-HDMI-Audio)
[    0.415798] ACPI: Added _OSI(Linux-HPI-Hybrid-Graphics)
[    0.416224] ACPI: 1 ACPI AML tables successfully acquired and loaded
[    0.419782] ACPI: Interpreter enabled
[    0.419800] ACPI: PM: (supports S0 S3 S4 S5)
[    0.419801] ACPI: Using IOAPIC for interrupt routing
[    0.419814] PCI: Using host bridge windows from ACPI; if necessary, use "pci=nocrs" and report a bug
[    0.419815] PCI: Using E820 reservations for host bridge windows
[    0.419895] ACPI: Enabled 2 GPEs in block 00 to 0F
[    0.422073] ACPI: PCI Root Bridge [PCI0] (domain 0000 [bus 00-ff])
[    0.422078] acpi PNP0A03:00: _OSC: OS supports [ASPM ClockPM Segments MSI HPX-Type3]
[    0.422080] acpi PNP0A03:00: _OSC: not requesting OS control; OS requires [ExtendedConfig ASPM ClockPM MSI]
[    0.422092] acpi PNP0A03:00: fail to add MMCONFIG information, can't access extended PCI configuration space under this bridge.
[    0.422163] PCI host bridge to bus 0000:00
[    0.422167] pci_bus 0000:00: root bus resource [io  0x0000-0x0cf7 window]
[    0.422169] pci_bus 0000:00: root bus resource [io  0x0d00-0xffff window]
[    0.422172] pci_bus 0000:00: root bus resource [mem 0x000a0000-0x000bffff window]
[    0.422173] pci_bus 0000:00: root bus resource [mem 0x80000000-0xfebfffff window]
[    0.422174] pci_bus 0000:00: root bus resource [mem 0x100000000-0x17fffffff window]
[    0.422176] pci_bus 0000:00: root bus resource [bus 00-ff]
[    0.422234] pci 0000:00:00.0: [8086:1237] type 00 class 0x060000
[    0.423452] pci 0000:00:01.0: [8086:7000] type 00 class 0x060100
[    0.424062] pci 0000:00:01.1: [8086:7010] type 00 class 0x010180
[    0.426640] pci 0000:00:01.1: reg 0x20: [io  0xc060-0xc06f]
[    0.427236] pci 0000:00:01.1: legacy IDE quirk: reg 0x10: [io  0x01f0-0x01f7]
[    0.427238] pci 0000:00:01.1: legacy IDE quirk: reg 0x14: [io  0x03f6]
[    0.427239] pci 0000:00:01.1: legacy IDE quirk: reg 0x18: [io  0x0170-0x0177]
[    0.427240] pci 0000:00:01.1: legacy IDE quirk: reg 0x1c: [io  0x0376]
[    0.430805] pci 0000:00:01.2: [8086:7020] type 00 class 0x0c0300
[    0.433271] pci 0000:00:01.2: reg 0x20: [io  0xc040-0xc05f]
[    0.437826] pci 0000:00:01.3: [8086:7113] type 00 class 0x068000
[    0.438294] pci 0000:00:01.3: quirk: [io  0x0600-0x063f] claimed by PIIX4 ACPI
[    0.438306] pci 0000:00:01.3: quirk: [io  0x0700-0x070f] claimed by PIIX4 SMB
[    0.438758] pci 0000:00:02.0: [1234:1111] type 00 class 0x030000
[    0.441685] pci 0000:00:02.0: reg 0x10: [mem 0xfd000000-0xfdffffff pref]
[    0.445666] pci 0000:00:02.0: reg 0x18: [mem 0xfebf0000-0xfebf0fff]
[    0.451036] pci 0000:00:02.0: reg 0x30: [mem 0xfebe0000-0xfebeffff pref]
[    0.465748] pci 0000:00:02.0: Video device with shadowed ROM at [mem 0x000c0000-0x000dffff]
[    0.467077] pci 0000:00:03.0: [8086:100e] type 00 class 0x020000
[    0.468639] pci 0000:00:03.0: reg 0x10: [mem 0xfebc0000-0xfebdffff]
[    0.469644] pci 0000:00:03.0: reg 0x14: [io  0xc000-0xc03f]
[    0.478093] pci 0000:00:03.0: reg 0x30: [mem 0xfeb80000-0xfebbffff pref]
[    0.492653] ACPI: PCI: Interrupt link LNKA configured for IRQ 10
[    0.492782] ACPI: PCI: Interrupt link LNKB configured for IRQ 10
[    0.492897] ACPI: PCI: Interrupt link LNKC configured for IRQ 11
[    0.493009] ACPI: PCI: Interrupt link LNKD configured for IRQ 11
[    0.493077] ACPI: PCI: Interrupt link LNKS configured for IRQ 9
[    0.493331] iommu: Default domain type: Translated 
[    0.493333] iommu: DMA domain TLB invalidation policy: lazy mode 
[    0.493472] SCSI subsystem initialized
[    0.498728] ACPI: bus type USB registered
[    0.498757] usbcore: registered new interface driver usbfs
[    0.498767] usbcore: registered new interface driver hub
[    0.498775] usbcore: registered new device driver usb
[    0.498792] pps_core: LinuxPPS API ver. 1 registered
[    0.498793] pps_core: Software ver. 5.3.6 - Copyright 2005-2007 Rodolfo Giometti <giometti@linux.it>
[    0.498796] PTP clock support registered
[    0.498943] Advanced Linux Sound Architecture Driver Initialized.
[    0.499174] NetLabel: Initializing
[    0.499174] NetLabel:  domain hash size = 128
[    0.499175] NetLabel:  protocols = UNLABELED CIPSOv4 CALIPSO
[    0.499197] NetLabel:  unlabeled traffic allowed by default
[    0.499295] PCI: Using ACPI for IRQ routing
[    0.499800] pci 0000:00:02.0: vgaarb: setting as boot VGA device
[    0.499805] pci 0000:00:02.0: vgaarb: bridge control possible
[    0.499808] pci 0000:00:02.0: vgaarb: VGA device added: decodes=io+mem,owns=io+mem,locks=none
[    0.499810] vgaarb: loaded
[    0.499866] hpet: 3 channels of 0 reserved for per-cpu timers
[    0.499890] hpet0: at MMIO 0xfed00000, IRQs 2, 8, 0
[    0.499894] hpet0: 3 comparators, 64-bit 100.000000 MHz counter
[    0.505681] clocksource: Switched to clocksource kvm-clock
[    0.505883] VFS: Disk quotas dquot_6.6.0
[    0.505897] VFS: Dquot-cache hash table entries: 512 (order 0, 4096 bytes)
[    0.505958] pnp: PnP ACPI init
[    0.506317] pnp: PnP ACPI: found 6 devices
[    0.512244] clocksource: acpi_pm: mask: 0xffffff max_cycles: 0xffffff, max_idle_ns: 2085701024 ns
[    0.517101] NET: Registered PF_INET protocol family
[    0.517428] IP idents hash table entries: 32768 (order: 6, 262144 bytes, linear)
[    0.520187] tcp_listen_portaddr_hash hash table entries: 1024 (order: 2, 16384 bytes, linear)
[    0.520194] TCP established hash table entries: 16384 (order: 5, 131072 bytes, linear)
[    0.520243] TCP bind bhash tables hash table entries: 16384 (order: 6, 393216 bytes, linear)
[    0.520292] TCP: Hash tables configured (established 16384 bind 16384)
[    0.520323] UDP hash table entries: 1024 (order: 3, 32768 bytes, linear)
[    0.520330] UDP-Lite hash table entries: 1024 (order: 3, 32768 bytes, linear)
[    0.520386] NET: Registered PF_UNIX/PF_LOCAL protocol family
[    0.523498] RPC: Registered named UNIX socket transport module.
[    0.523499] RPC: Registered udp transport module.
[    0.523499] RPC: Registered tcp transport module.
[    0.523500] RPC: Registered tcp NFSv4.1 backchannel transport module.
[    0.523697] pci_bus 0000:00: resource 4 [io  0x0000-0x0cf7 window]
[    0.523701] pci_bus 0000:00: resource 5 [io  0x0d00-0xffff window]
[    0.523702] pci_bus 0000:00: resource 6 [mem 0x000a0000-0x000bffff window]
[    0.523703] pci_bus 0000:00: resource 7 [mem 0x80000000-0xfebfffff window]
[    0.523704] pci_bus 0000:00: resource 8 [mem 0x100000000-0x17fffffff window]
[    0.523748] pci 0000:00:01.0: PIIX3: Enabling Passive Release
[    0.523762] pci 0000:00:00.0: Limiting direct PCI/PCI transfers
[    0.523775] pci 0000:00:01.0: Activating ISA DMA hang workarounds
[    0.550942] ACPI: \_SB_.LNKD: Enabled at IRQ 11
[    0.581379] pci 0000:00:01.2: quirk_usb_early_handoff+0x0/0x700 took 56231 usecs
[    0.581450] PCI: CLS 0 bytes, default 64
[    0.581561] clocksource: tsc: mask: 0xffffffffffffffff max_cycles: 0x23f44f92df3, max_idle_ns: 440795329494 ns
[    0.583061] kworker/u2:2 (37) used greatest stack depth: 14736 bytes left
[    0.583142] kworker/u2:2 (41) used greatest stack depth: 14632 bytes left
[    0.624401] Initialise system trusted keyrings
[    0.625545] workingset: timestamp_bits=56 max_order=19 bucket_order=0
[    0.627827] NFS: Registering the id_resolver key type
[    0.627837] Key type id_resolver registered
[    0.627838] Key type id_legacy registered
[    0.627885] 9p: Installing v9fs 9p2000 file system support
[    0.640475] Key type asymmetric registered
[    0.640479] Asymmetric key parser 'x509' registered
[    0.640514] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 251)
[    0.640538] io scheduler mq-deadline registered
[    0.640539] io scheduler kyber registered
[    0.644249] input: Power Button as /devices/LNXSYSTM:00/LNXPWRBN:00/input/input0
[    0.644325] ACPI: button: Power Button [PWRF]
[    0.647302] Serial: 8250/16550 driver, 4 ports, IRQ sharing enabled
[    0.647463] 00:04: ttyS0 at I/O 0x3f8 (irq = 4, base_baud = 115200) is a 16550A
[    0.648223] Non-volatile memory driver v1.3
[    0.648225] Linux agpgart interface v0.103
[    0.648356] ACPI: bus type drm_connector registered
[    0.650136] loop: module loaded
[    0.651063] kworker/u2:3 (81) used greatest stack depth: 14464 bytes left
[    0.656217] scsi host0: ata_piix
[    0.657174] scsi host1: ata_piix
[    0.657203] ata1: PATA max MWDMA2 cmd 0x1f0 ctl 0x3f6 bmdma 0xc060 irq 14
[    0.657205] ata2: PATA max MWDMA2 cmd 0x170 ctl 0x376 bmdma 0xc068 irq 15
[    0.657396] e100: Intel(R) PRO/100 Network Driver
[    0.657397] e100: Copyright(c) 1999-2006 Intel Corporation
[    0.657403] e1000: Intel(R) PRO/1000 Network Driver
[    0.657404] e1000: Copyright (c) 1999-2006 Intel Corporation.
[    0.684332] ACPI: \_SB_.LNKC: Enabled at IRQ 10
[    0.819405] ata2: found unknown device (class 0)
[    0.819751] ata1: found unknown device (class 0)
[    0.821925] ata1.00: ATA-7: QEMU HARDDISK, 2.5+, max UDMA/100
[    0.821928] ata1.00: 65536 sectors, multi 16: LBA48 
[    0.822475] scsi 0:0:0:0: Direct-Access     ATA      QEMU HARDDISK    2.5+ PQ: 0 ANSI: 5
[    0.822803] sd 0:0:0:0: [sda] 65536 512-byte logical blocks: (33.6 MB/32.0 MiB)
[    0.822813] sd 0:0:0:0: [sda] Write Protect is off
[    0.822826] sd 0:0:0:0: [sda] Write cache: enabled, read cache: enabled, doesn't support DPO or FUA
[    0.822842] sd 0:0:0:0: [sda] Preferred minimum I/O size 512 bytes
[    0.822987] sd 0:0:0:0: Attached scsi generic sg0 type 0
[    0.823786] sd 0:0:0:0: [sda] Attached SCSI disk
[    0.823949] ata2.00: ATAPI: QEMU DVD-ROM, 2.5+, max UDMA/100
[    0.825846] scsi 1:0:0:0: CD-ROM            QEMU     QEMU DVD-ROM     2.5+ PQ: 0 ANSI: 5
[    0.867519] sr 1:0:0:0: [sr0] scsi3-mmc drive: 4x/4x cd/rw xa/form2 tray
[    0.867535] cdrom: Uniform CD-ROM driver Revision: 3.20
[    0.890288] sr 1:0:0:0: Attached scsi generic sg1 type 5
[    1.031184] e1000 0000:00:03.0 eth0: (PCI:33MHz:32-bit) 52:54:00:12:34:56
[    1.031195] e1000 0000:00:03.0 eth0: Intel(R) PRO/1000 Network Connection
[    1.031243] e1000e: Intel(R) PRO/1000 Network Driver
[    1.031244] e1000e: Copyright(c) 1999 - 2015 Intel Corporation.
[    1.031274] sky2: driver version 1.30
[    1.031480] ehci_hcd: USB 2.0 'Enhanced' Host Controller (EHCI) Driver
[    1.031482] ehci-pci: EHCI PCI platform driver
[    1.031497] ohci_hcd: USB 1.1 'Open' Host Controller (OHCI) Driver
[    1.031514] ohci-pci: OHCI PCI platform driver
[    1.031527] uhci_hcd: USB Universal Host Controller Interface driver
[    1.056915] uhci_hcd 0000:00:01.2: UHCI Host Controller
[    1.057002] uhci_hcd 0000:00:01.2: new USB bus registered, assigned bus number 1
[    1.057162] uhci_hcd 0000:00:01.2: irq 11, io port 0x0000c040
[    1.057283] usb usb1: New USB device found, idVendor=1d6b, idProduct=0001, bcdDevice= 5.19
[    1.057285] usb usb1: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    1.057286] usb usb1: Product: UHCI Host Controller
[    1.057287] usb usb1: Manufacturer: Linux 5.19.0-rc1-next-20220609 uhci_hcd
[    1.057288] usb usb1: SerialNumber: 0000:00:01.2
[    1.057418] hub 1-0:1.0: USB hub found
[    1.057429] hub 1-0:1.0: 2 ports detected
[    1.059979] usbcore: registered new interface driver usblp
[    1.060008] usbcore: registered new interface driver usb-storage
[    1.060088] i8042: PNP: PS/2 Controller [PNP0303:KBD,PNP0f13:MOU] at 0x60,0x64 irq 1,12
[    1.063620] serio: i8042 KBD port at 0x60,0x64 irq 1
[    1.063634] serio: i8042 AUX port at 0x60,0x64 irq 12
[    1.064176] input: AT Translated Set 2 keyboard as /devices/platform/i8042/serio0/input/input1
[    1.066670] rtc_cmos 00:05: RTC can wake from S4
[    1.067366] rtc_cmos 00:05: registered as rtc0
[    1.067404] rtc_cmos 00:05: alarms up to one day, y3k, 242 bytes nvram, hpet irqs
[    1.067632] device-mapper: ioctl: 4.46.0-ioctl (2022-02-22) initialised: dm-devel@redhat.com
[    1.067662] intel_pstate: CPU model not supported
[    1.067707] hid: raw HID events driver (C) Jiri Kosina
[    1.068505] usbcore: registered new interface driver usbhid
[    1.068507] usbhid: USB HID core driver
[    1.069268] Initializing XFRM netlink socket
[    1.069354] NET: Registered PF_INET6 protocol family
[    1.078054] Segment Routing with IPv6
[    1.078065] In-situ OAM (IOAM) with IPv6
[    1.078109] sit: IPv6, IPv4 and MPLS over IPv4 tunneling driver
[    1.078246] NET: Registered PF_PACKET protocol family
[    1.078290] 9pnet: Installing 9P2000 support
[    1.078308] Key type dns_resolver registered
[    1.078525] IPI shorthand broadcast: enabled
[    1.078548] sched_clock: Marking stable (1061935413, 15986430)->(1175594581, -97672738)
[    1.078632] registered taskstats version 1
[    1.078652] Loading compiled-in X.509 certificates
[    1.103143] PM:   Magic number: 6:507:177
[    1.103173] acpi device:0d: hash matches
[    1.103208] printk: console [netcon0] enabled
[    1.103209] netconsole: network logging started
[    1.103292] cfg80211: Loading compiled-in X.509 certificates for regulatory database
[    1.103341] printk: console [netcon0] printing thread started
[    1.103756] cfg80211: Loaded X.509 cert 'sforshee: 00b28ddf47aef9cea7'
[    1.103773] ALSA device list:
[    1.103776]   No soundcards found.
[    1.105819] platform regulatory.0: Direct firmware load for regulatory.db failed with error -2
[    1.105826] cfg80211: failed to load regulatory.db
[    1.691327] input: ImExPS/2 Generic Explorer Mouse as /devices/platform/i8042/serio1/input/input3
[    1.694237] md: Waiting for all devices to be available before autodetect
[    1.694238] md: If you don't use raid, use raid=noautodetect
[    1.694248] md: Autodetecting RAID arrays.
[    1.694248] md: autorun ...
[    1.694249] md: ... autorun DONE.
[    1.826039] EXT4-fs (sda): recovery complete
[    1.836626] EXT4-fs (sda): mounted filesystem with ordered data mode. Quota mode: none.
[    1.836811] VFS: Mounted root (ext4 filesystem) on device 8:0.
[    1.846423] devtmpfs: mounted
[    1.848631] Freeing unused kernel image (initmem) memory: 1272K
[    1.849417] Write protecting the kernel read-only data: 24576k
[    1.854076] Freeing unused kernel image (text/rodata gap) memory: 2032K
[    1.862144] Freeing unused kernel image (rodata/data gap) memory: 928K
[    1.862232] Run /init as init process
[    1.880176] process '/bin/busybox' started with executable stack
[    1.914123] mount (342) used greatest stack depth: 13856 bytes left
/bin/sh: can't access tty; job control turned off
~ # 
~ # 