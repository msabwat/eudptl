// SPDX-License-Identifier: GPL-2.0-only
/*
 *
 *
 *  Copyright (C) 2022  Mehdi Sabwat
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/delay.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Louis Solofrizzo <louis@ne02ptzero.me>");
MODULE_DESCRIPTION("Module doing work.");
MODULE_VERSION("0.1");

int do_work(int *my_int)
{
	int x;
	int y = *my_int;

	for (x = 0; x < y; ++x)
		usleep_range(9, 11);
	if (y < 10)
		/* That was a long sleep, tell userspace about it */
		pr_info("We slept a long time!");
	return x * y;
}

int my_init(void)
{
	int x = 10;

	x = do_work(&x);
	pr_info("Work returned value: %d\n", x);
	return 0;
}

void my_exit(void)
{
	pr_info("destroying work module\n");
}

module_init(my_init);
module_exit(my_exit);
