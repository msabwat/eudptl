#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <assert.h>

main()
{
	int fd = open("/dev/fortytwo", O_RDWR);
	if (fd == -1) {
		printf("open: failed!\n");
		return 1;
	}
	printf("open: success!\n");
	int ret = write(fd, "write", 5);
	assert(ret == -1);
	ret = write(fd, "msabwat", 7);
	assert(ret == 7);
	char buf[8];
	ret = read(fd, buf, 7);
	buf[7] = '\0';
	printf("read:%s size:%d from device\n", buf, ret);
	return 0;
}
