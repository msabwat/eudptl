// SPDX-License-Identifier: GPL-2.0-only
/*
 *
 *
 *  Copyright (C) 2022  Mehdi Sabwat
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/string.h>
#include <asm/errno.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mehdi Sabwat <mehdisabwat@gmail.com>");
MODULE_DESCRIPTION("42 misc char device driver.");
MODULE_VERSION("0.1");

static ssize_t fortytwo_read(struct file *file, char __user *buf, size_t size,
			     loff_t *pos)
{
	char *kernel_buf = "msabwat";

	return simple_read_from_buffer(buf, size, pos, kernel_buf, 7);
}

static ssize_t fortytwo_write(struct file *file, const char __user *buf,
			      size_t size, loff_t *pos)
{
	char kernel_buf[7];

	simple_write_to_buffer(kernel_buf, 7, pos, buf, size);
	if (strncmp(kernel_buf, "msabwat", 7) != 0) {
		pr_err("fortytwo: error, invalid value!\n");
		return -EINVAL;
	}
	return 7;
}

static int fortytwo_open(struct inode *inode, struct file *file)
{
	/* I don't need to initialize private data (my_data) */
	//       struct my_device_data *my_data =
	// container_of(inode->i_cdev, struct my_data, cdev);
	pr_info("opening fortytwo misc device with inode:%lu\n", inode->i_ino);
	return 0;
}

static int fortytwo_release(struct inode *inode, struct file *file)
{
	pr_info("closing fortytwo misc device with inode:%lu\n", inode->i_ino);
	return 0;
}

static const struct file_operations fops = {
	.owner		= THIS_MODULE,
	.write		= fortytwo_write,
	.read		= fortytwo_read,
	.open		= fortytwo_open,
	.release	= fortytwo_release,
};

struct miscdevice fortytwo_device = {
	.minor	= MISC_DYNAMIC_MINOR,
	.name	= "fortytwo",
	.fops	= &fops,
};

static int __init fortytwo_init(void)
{
	int ret = misc_register(&fortytwo_device);

	if (ret != 0) {
		pr_err("misc_register init failed!\n");
		return ret;
	}
	pr_info("fortytwo: misc_register init done!\n");
	return 0;
}

static void __exit fortytwo_exit(void)
{
	misc_deregister(&fortytwo_device);
	pr_info("fortytwo: misc_register exit done!\n");
}

module_init(fortytwo_init);
module_exit(fortytwo_exit);
