// SPDX-License-Identifier: GPL-2.0-only
/*
 *
 *
 *  Copyright (C) 2022  Mehdi Sabwat
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/debugfs.h>
#include <linux/string.h>
#include <linux/jiffies.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#include <asm/errno.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mehdi Sabwat <mehdisabwat@gmail.com>");
MODULE_DESCRIPTION("debugfs with 3 virtual files.");
MODULE_VERSION("0.1");

// mutex for the foo file, because it is shared between users.
DEFINE_MUTEX(debugfs_foo_mutex);
static struct dentry *dir_handle;
static char data[PAGE_SIZE];
static int data_len;

static ssize_t read_id_file(struct file *file, char __user *buf, size_t size,
			    loff_t *pos)
{
	char *kernel_buf = "msabwat";

	return simple_read_from_buffer(buf, size, pos, kernel_buf, 7);
}

static ssize_t write_id_file(struct file *file, const char __user *buf,
			     size_t size, loff_t *pos)
{
	char kernel_buf[7];

	if (copy_from_user(kernel_buf, buf, 7))
		return -EFAULT;
	if (strncmp(kernel_buf, "msabwat", 7) != 0) {
		pr_err("fortytwo: error, invalid value!\n");
		return -EINVAL;
	}
	return 7;
}

static ssize_t read_jiffies_file(struct file *file, char __user *buf,
				 size_t size, loff_t *pos)
{
	char tmp[12]; // ulong_max + separator
	int ret = snprintf(tmp, 12, "%lu\n", jiffies);

	return simple_read_from_buffer(buf, size, pos, tmp, ret);
}

static ssize_t read_foo_file(struct file *file, char __user *buf, size_t size,
			     loff_t *pos)
{
	int ret = 0;

	if (mutex_lock_interruptible(&debugfs_foo_mutex))
		return -EINTR;
	ret = simple_read_from_buffer(buf, size, pos, data, data_len);
	mutex_unlock(&debugfs_foo_mutex);
	return ret;
}

static ssize_t write_foo_file(struct file *file, const char __user *buf,
			      size_t size, loff_t *pos)
{
	ssize_t cnt = 0;

	if (mutex_lock_interruptible(&debugfs_foo_mutex))
		return -EINTR;
	data_len = size;
	if (data_len > PAGE_SIZE)
		data_len = PAGE_SIZE;
	cnt = simple_write_to_buffer(data, data_len, pos, buf, size);
	if (cnt != data_len)
		cnt = -EFAULT;
	mutex_unlock(&debugfs_foo_mutex);
	return cnt;
}

static const struct file_operations id_fops = {
	.owner	= THIS_MODULE,
	.read	= read_id_file,
	.write	= write_id_file,
};

static const struct file_operations jiffies_fops = {
	.owner	= THIS_MODULE,
	.read	= read_jiffies_file,
};

static const struct file_operations foo_fops = {
	.owner	= THIS_MODULE,
	.read	= read_foo_file,
	.write	= write_foo_file,
};

static int __init debugfs_init(void)
{
	struct dentry *f_handle;

	dir_handle = debugfs_create_dir("fortytwo", NULL);

	if (!dir_handle)
		return -ENOENT;

	f_handle = debugfs_create_file("id", 0666, dir_handle, NULL, &id_fops);
	if (!f_handle)
		goto exit;
	f_handle = debugfs_create_file("jiffies", 0444, dir_handle, NULL,
				       &jiffies_fops);
	if (!f_handle)
		goto exit;
	f_handle =
		debugfs_create_file("foo", 0644, dir_handle, NULL, &foo_fops);
	if (!f_handle)
		goto exit;
	pr_info("debugfs: debugfs init done!\n");
	return 0;
exit:
	debugfs_remove_recursive(dir_handle);
	return -ENOENT;
}

static void __exit debugfs_exit(void)
{
	pr_info("debugfs: recursively removing debugfs/ !\n");
	debugfs_remove_recursive(dir_handle);
	pr_info("debugfs: debugfs exit done!\n");
}

module_init(debugfs_init);
module_exit(debugfs_exit);
