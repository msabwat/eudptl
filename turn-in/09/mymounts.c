// SPDX-License-Identifier: GPL-3.0-only
/*
 *
 *
 *  Copyright (C) 2022  Mehdi Sabwat
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/fs_struct.h>
#include <linux/string.h>
#include <linux/namei.h>
#include <asm/errno.h>
#include <linux/seq_file.h>
#include <linux/dcache.h>
#include <linux/path.h>
#include <linux/types.h>
#include <../fs/mount.h> // pas glop

/*
 * - use collect/iterate mounts, we would need to load the kernel
 * symbols because they are not exported.
 * (pas propre et plus possible à
 * cause de 0bd476e6c67190b5eb7b6e105c8db8ff61103281)
 *
 * - use the current namespace proxy. In it, we have the mnt_ns
 * that has a list of mountpoints.
 * cette solution est mauvaise parce qu'on ne peut pas
 * accéder à namespace_sem et on en a besoin d'après la doc.

 * - Dcache: issue is you don't need to traverse all the directories
 * to find mountpoints if they are in the superblock struct.

 * Disclaimer:
 * the module will only work with the sources (not the ones in /usr/src/linux,
 * it cannot be tested in a host kernel.
 */
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mehdi Sabwat <mehdisabwat@gmail.com>");
MODULE_DESCRIPTION("file showing the list of mounts");
MODULE_VERSION("0.1");

static struct proc_dir_entry *proc;
/*
 *fonction récursive qui permet de parcourir les points
 *de montage d'un struct mount

 *La fonction ne couvre pas encore tous les types de mounts:
 *https://www.kernel.org/doc/Documentation/filesystems/sharedsubtree.txt
 */
static void my_mounts_walk(struct seq_file *s, struct mount *mnt)
{
	struct mount *c_mnt;
	char buf[64];
	struct path path;
	char *chemin	= NULL;

	memset(buf, 0, sizeof(buf));
	path.mnt	= &mnt->mnt;
	path.dentry	= mnt->mnt.mnt_root;

	if (!path_has_submounts(&path))
		return;
	list_for_each_entry(c_mnt, &mnt->mnt_mounts, mnt_child) {
		path.mnt = &c_mnt->mnt;
		path.dentry = c_mnt->mnt.mnt_root;
		chemin = d_path(&path, buf, sizeof(buf));
		seq_printf(s, "%-10s%-64s\n",
			   c_mnt->mnt_mountpoint->d_name.name, chemin);
		// attempt to load child mounts, bail out if it's not possible
		my_mounts_walk(s, c_mnt);
	}
}

static int get_mounts(struct seq_file *s, void *p)
{
	/* this function will fill the seqfile
	 * task_struct is a structure used by the kernel scheduler,
	 * it provides, a per-process context to our module. (current).
	 * Inside it, there is the `struct fs_struct` field, that gives
	 * us access to the `struct path` field of the root element.
	 */
	struct fs_struct *fs	= current->fs;
	struct vfsmount *vfsmnt = fs->root.mnt;
	struct mount *mnt;

	/*
	 * the root filesystem will always be root, and path
	 * will always be "/"
	 */
	seq_puts(s, "root      /\n");
	list_for_each_entry(mnt, &vfsmnt->mnt_sb->s_mounts, mnt_instance) {
		my_mounts_walk(s, mnt);
	}
	return 0;
}

static int open_mounts(struct inode *i, struct file *f)
{
	return single_open(f, get_mounts, NULL);
}

static const struct proc_ops ops = {
	.proc_open	= open_mounts,
	.proc_read	= seq_read,
	.proc_lseek	= seq_lseek,
	.proc_release	= seq_release,
};

static int __init mymounts_init(void)
{
	proc = proc_create("mymounts", 0444, NULL, &ops);
	pr_info("mymounts: opening mymounts module");
	return 0;
}

static void __exit mymounts_exit(void)
{
	proc_remove(proc);
	pr_info("mymounts: removed mymounts module");
}

module_init(mymounts_init);
module_exit(mymounts_exit);
