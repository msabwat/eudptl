// SPDX-License-Identifier: GPL-2.0-only
/*
 *
 *
 *  Copyright (C) 2022  Mehdi Sabwat
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mehdi Sabwat <mehdisabwat@gmail.com>");
MODULE_DESCRIPTION("LP hello module.");
MODULE_VERSION("0.1");

static int __init hello(void)
{
	pr_info("Hello world !\n");
	return 0;
}

static void __exit bye(void)
{
	pr_info("Cleaning up module.\n");
}

module_init(hello);
module_exit(bye);
