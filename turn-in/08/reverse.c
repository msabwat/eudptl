// SPDX-License-Identifier: GPL-2.0-only
/*
 *
 *
 *  Copyright (C) 2022  Mehdi Sabwat
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/slab.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Louis Solofrizzo <louis@ne02ptzero.me>");
MODULE_DESCRIPTION("Reverse string written to misc device");
MODULE_VERSION("0.1");

// mutex for the input buffer
DEFINE_MUTEX(buf_mutex);
static char str[PAGE_SIZE * 2];
static size_t len;

ssize_t myfd_read(struct file *fp, char __user *user, size_t size, loff_t *offs)
{
	ssize_t ret = 0;
	char *tmp;
	size_t i = 0;

	tmp = kmalloc(sizeof(char) * PAGE_SIZE * 2, GFP_KERNEL);
	if (mutex_lock_interruptible(&buf_mutex))
		return -EINTR;
	for (i = 0; i < len; i++)
		tmp[i] = str[len - i - 1];

	tmp[i] = '\0';
	ret = simple_read_from_buffer(user, size, offs, tmp, len);
	mutex_unlock(&buf_mutex);
	kfree(tmp);
	return ret;
}

ssize_t myfd_write(struct file *fp, const char __user *user, size_t size,
		   loff_t *offs)
{
	ssize_t ret = 0;

	if (mutex_lock_interruptible(&buf_mutex))
		return -EINTR;
	len = size;
	if (len > PAGE_SIZE)
		len = PAGE_SIZE;
	ret = simple_write_to_buffer(str, len, offs, user, size);
	if (ret != len)
		ret = -EFAULT;
	mutex_unlock(&buf_mutex);
	return ret;
}

static const struct file_operations myfd_fops = {
	.owner	= THIS_MODULE,
	.read	= &myfd_read,
	.write	= &myfd_write
};

static struct miscdevice myfd_device = {
	.minor	= MISC_DYNAMIC_MINOR,
	.name	= "reverse",
	.fops	= &myfd_fops
};

static int __init myfd_init(void)
{
	int ret = misc_register(&myfd_device);

	if (ret != 0) {
		pr_err("init failed!\n");
		return ret;
	}

	pr_info("init done\n");
	return 0;
}

static void __exit myfd_cleanup(void)
{
	misc_deregister(&myfd_device);
	pr_info("exit done!\n");
}

module_init(myfd_init);
module_exit(myfd_cleanup);
