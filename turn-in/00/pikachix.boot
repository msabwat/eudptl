c[?7l[2J[0mSeaBIOS (version ArchLinux 1.15.0-1)


iPXE (http://ipxe.org) 00:03.0 CA00 PCI2.10 PnP PMM+7FF91350+7FEF1350 CA00
Press Ctrl-B to configure iPXE (PCI 00:03.0)...                                                                               


Booting from ROM..c[?7l[2J[0m.No EFI environment detected.
early console in extract_kernel
input_data: 0x0000000002b7140d
input_len: 0x0000000000a9c502
output: 0x0000000001000000
output_len: 0x00000000025c7230
kernel_total_size: 0x0000000002230000
needed_size: 0x0000000002600000
trampoline_32bit: 0x000000000009d000
Physical KASLR using RDTSC...
Virtual KASLR using RDTSC...

Decompressing Linux... Parsing ELF... Performing relocations... done.
Booting the kernel.
[    0.000000] Linux version 6.0.0-rc1-g15b3f48a4339 (b1ue@b1uesea) (gcc (GCC) 11.2.0, GNU ld (GNU Binutils) 2.38) #1 SMP PREEMPT_DYNAMIC Sun Aug 21 21:26:23 CEST 2022
[    0.000000] Command line: earlyprintk=serial,ttyS0 console=ttyS0 init=/init root=/dev/sda
[    0.000000] x86/fpu: x87 FPU will use FXSAVE
[    0.000000] signal: max sigframe size: 1440
[    0.000000] BIOS-provided physical RAM map:
[    0.000000] BIOS-e820: [mem 0x0000000000000000-0x000000000009fbff] usable
[    0.000000] BIOS-e820: [mem 0x000000000009fc00-0x000000000009ffff] reserved
[    0.000000] BIOS-e820: [mem 0x00000000000f0000-0x00000000000fffff] reserved
[    0.000000] BIOS-e820: [mem 0x0000000000100000-0x000000007ffdffff] usable
[    0.000000] BIOS-e820: [mem 0x000000007ffe0000-0x000000007fffffff] reserved
[    0.000000] BIOS-e820: [mem 0x00000000feffc000-0x00000000feffffff] reserved
[    0.000000] BIOS-e820: [mem 0x00000000fffc0000-0x00000000ffffffff] reserved
[    0.000000] printk: bootconsole [earlyser0] enabled
[    0.000000] NX (Execute Disable) protection: active
[    0.000000] SMBIOS 2.8 present.
[    0.000000] DMI: QEMU Standard PC (i440FX + PIIX, 1996), BIOS ArchLinux 1.15.0-1 04/01/2014
[    0.000000] Hypervisor detected: KVM
[    0.000000] kvm-clock: Using msrs 4b564d01 and 4b564d00
[    0.000003] kvm-clock: using sched offset of 420540357 cycles
[    0.000945] clocksource: kvm-clock: mask: 0xffffffffffffffff max_cycles: 0x1cd42e4dffb, max_idle_ns: 881590591483 ns
[    0.003817] tsc: Detected 2494.332 MHz processor
[    0.005467] last_pfn = 0x7ffe0 max_arch_pfn = 0x400000000
[    0.006425] x86/PAT: Configuration [0-7]: WB  WC  UC- UC  WB  WP  UC- WT  
Memory KASLR using RDTSC...
[    0.016919] found SMP MP-table at [mem 0x000f5ba0-0x000f5baf]
[    0.018421] ACPI: Early table checksum verification disabled
[    0.019407] ACPI: RSDP 0x00000000000F59E0 000014 (v00 BOCHS )
[    0.020443] ACPI: RSDT 0x000000007FFE1905 000034 (v01 BOCHS  BXPC     00000001 BXPC 00000001)
[    0.021861] ACPI: FACP 0x000000007FFE17B9 000074 (v01 BOCHS  BXPC     00000001 BXPC 00000001)
[    0.023269] ACPI: DSDT 0x000000007FFE0040 001779 (v01 BOCHS  BXPC     00000001 BXPC 00000001)
[    0.024896] ACPI: FACS 0x000000007FFE0000 000040
[    0.025852] ACPI: APIC 0x000000007FFE182D 000078 (v01 BOCHS  BXPC     00000001 BXPC 00000001)
[    0.027417] ACPI: HPET 0x000000007FFE18A5 000038 (v01 BOCHS  BXPC     00000001 BXPC 00000001)
[    0.029385] ACPI: WAET 0x000000007FFE18DD 000028 (v01 BOCHS  BXPC     00000001 BXPC 00000001)
[    0.031044] ACPI: Reserving FACP table memory at [mem 0x7ffe17b9-0x7ffe182c]
[    0.032436] ACPI: Reserving DSDT table memory at [mem 0x7ffe0040-0x7ffe17b8]
[    0.033696] ACPI: Reserving FACS table memory at [mem 0x7ffe0000-0x7ffe003f]
[    0.034853] ACPI: Reserving APIC table memory at [mem 0x7ffe182d-0x7ffe18a4]
[    0.036009] ACPI: Reserving HPET table memory at [mem 0x7ffe18a5-0x7ffe18dc]
[    0.037230] ACPI: Reserving WAET table memory at [mem 0x7ffe18dd-0x7ffe1904]
[    0.038734] No NUMA configuration found
[    0.039375] Faking a node at [mem 0x0000000000000000-0x000000007ffdffff]
[    0.040616] NODE_DATA(0) allocated [mem 0x7ffdc000-0x7ffdffff]
[    0.041797] Zone ranges:
[    0.042309]   DMA      [mem 0x0000000000001000-0x0000000000ffffff]
[    0.043548]   DMA32    [mem 0x0000000001000000-0x000000007ffdffff]
[    0.044757]   Normal   empty
[    0.045238] Movable zone start for each node
[    0.045942] Early memory node ranges
[    0.046533]   node   0: [mem 0x0000000000001000-0x000000000009efff]
[    0.047652]   node   0: [mem 0x0000000000100000-0x000000007ffdffff]
[    0.048700] Initmem setup node 0 [mem 0x0000000000001000-0x000000007ffdffff]
[    0.050152] On node 0, zone DMA: 1 pages in unavailable ranges
[    0.050178] On node 0, zone DMA: 97 pages in unavailable ranges
[    0.057790] On node 0, zone DMA32: 32 pages in unavailable ranges
[    0.059363] ACPI: PM-Timer IO Port: 0x608
[    0.061166] ACPI: LAPIC_NMI (acpi_id[0xff] dfl dfl lint[0x1])
[    0.062219] IOAPIC[0]: apic_id 0, version 17, address 0xfec00000, GSI 0-23
[    0.063695] ACPI: INT_SRC_OVR (bus 0 bus_irq 0 global_irq 2 dfl dfl)
[    0.064814] ACPI: INT_SRC_OVR (bus 0 bus_irq 5 global_irq 5 high level)
[    0.066069] ACPI: INT_SRC_OVR (bus 0 bus_irq 9 global_irq 9 high level)
[    0.067843] ACPI: INT_SRC_OVR (bus 0 bus_irq 10 global_irq 10 high level)
[    0.069859] ACPI: INT_SRC_OVR (bus 0 bus_irq 11 global_irq 11 high level)
[    0.071514] ACPI: Using ACPI (MADT) for SMP configuration information
[    0.072587] ACPI: HPET id: 0x8086a201 base: 0xfed00000
[    0.073467] smpboot: Allowing 1 CPUs, 0 hotplug CPUs
[    0.074511] PM: hibernation: Registered nosave memory: [mem 0x00000000-0x00000fff]
[    0.076160] PM: hibernation: Registered nosave memory: [mem 0x0009f000-0x0009ffff]
[    0.077521] PM: hibernation: Registered nosave memory: [mem 0x000a0000-0x000effff]
[    0.078776] PM: hibernation: Registered nosave memory: [mem 0x000f0000-0x000fffff]
[    0.080113] [mem 0x80000000-0xfeffbfff] available for PCI devices
[    0.081241] Booting paravirtualized kernel on KVM
[    0.082045] clocksource: refined-jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 1910969940391419 ns
[    0.090091] setup_percpu: NR_CPUS:64 nr_cpumask_bits:64 nr_cpu_ids:1 nr_node_ids:1
[    0.091972] percpu: Embedded 52 pages/cpu s176040 r8192 d28760 u2097152
[    0.093154] Fallback order for Node 0: 0 
[    0.093893] Built 1 zonelists, mobility grouping on.  Total pages: 515808
[    0.095030] Policy zone: DMA32
[    0.095540] Kernel command line: earlyprintk=serial,ttyS0 console=ttyS0 init=/init root=/dev/sda
[    0.097770] Dentry cache hash table entries: 262144 (order: 9, 2097152 bytes, linear)
[    0.099321] Inode-cache hash table entries: 131072 (order: 8, 1048576 bytes, linear)
[    0.100844] mem auto-init: stack:off, heap alloc:off, heap free:off
[    0.107412] Memory: 2024744K/2096632K available (16396K kernel code, 2662K rwdata, 5164K rodata, 1528K init, 936K bss, 71628K reserved, 0K cma-reserved)
[    0.111153] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=1, Nodes=1
[    0.112899] Kernel/User page tables isolation: enabled
[    0.116260] Dynamic Preempt: voluntary
[    0.117213] rcu: Preemptible hierarchical RCU implementation.
[    0.118187] rcu: 	RCU event tracing is enabled.
[    0.118924] rcu: 	RCU restricting CPUs from NR_CPUS=64 to nr_cpu_ids=1.
[    0.120208] 	Trampoline variant of Tasks RCU enabled.
[    0.121134] rcu: RCU calculated value of scheduler-enlistment delay is 100 jiffies.
[    0.122382] rcu: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=1
[    0.125010] NR_IRQS: 4352, nr_irqs: 256, preallocated irqs: 16
[    0.126436] rcu: srcu_init: Setting srcu_struct sizes based on contention.
[    0.134261] Console: colour VGA+ 80x25
[    0.134951] printk: console [ttyS0] enabled
[    0.134951] printk: console [ttyS0] enabled
[    0.136364] printk: bootconsole [earlyser0] disabled
[    0.136364] printk: bootconsole [earlyser0] disabled
[    0.138136] ACPI: Core revision 20220331
[    0.139004] clocksource: hpet: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 19112604467 ns
[    0.140736] APIC: Switch to symmetric I/O mode setup
[    0.143300] ..TIMER: vector=0x30 apic1=0 pin1=2 apic2=-1 pin2=-1
[    0.145176] clocksource: tsc-early: mask: 0xffffffffffffffff max_cycles: 0x23f44f92df3, max_idle_ns: 440795329494 ns
[    0.147060] Calibrating delay loop (skipped) preset value.. 4988.66 BogoMIPS (lpj=2494332)
[    0.148055] pid_max: default: 32768 minimum: 301
[    0.149083] LSM: Security Framework initializing
[    0.150077] SELinux:  Initializing.
[    0.151163] Mount-cache hash table entries: 4096 (order: 3, 32768 bytes, linear)
[    0.152066] Mountpoint-cache hash table entries: 4096 (order: 3, 32768 bytes, linear)
Poking KASLR using RDTSC...
[    0.153945] Last level iTLB entries: 4KB 0, 2MB 0, 4MB 0
[    0.154058] Last level dTLB entries: 4KB 0, 2MB 0, 4MB 0, 1GB 0
[    0.155085] Spectre V1 : Mitigation: usercopy/swapgs barriers and __user pointer sanitization
[    0.156058] Spectre V2 : Mitigation: Retpolines
[    0.156831] Spectre V2 : Spectre v2 / SpectreRSB mitigation: Filling RSB on context switch
[    0.157058] Spectre V2 : Spectre v2 / SpectreRSB : Filling RSB on VMEXIT
[    0.158058] Speculative Store Bypass: Vulnerable
[    0.158842] MDS: Vulnerable: Clear CPU buffers attempted, no microcode
[    0.187708] Freeing SMP alternatives memory: 44K
[    0.291053] smpboot: CPU0: Intel QEMU Virtual CPU version 2.5+ (family: 0xf, model: 0x6b, stepping: 0x1)
[    0.292140] cblist_init_generic: Setting adjustable number of callback queues.
[    0.293054] cblist_init_generic: Setting shift to 0 and lim to 1.
[    0.294075] Performance Events: unsupported Netburst CPU model 107 no PMU driver, software events only.
[    0.295156] rcu: Hierarchical SRCU implementation.
[    0.296060] rcu: 	Max phase no-delay instances is 400.
[    0.297656] smp: Bringing up secondary CPUs ...
[    0.298061] smp: Brought up 1 node, 1 CPU
[    0.299055] smpboot: Max logical packages: 1
[    0.300055] smpboot: Total of 1 processors activated (4988.66 BogoMIPS)
[    0.301282] devtmpfs: initialized
[    0.302248] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 1911260446275000 ns
[    0.303058] futex hash table entries: 256 (order: 2, 16384 bytes, linear)
[    0.304151] PM: RTC time: 21:57:50, date: 2022-08-21
[    0.305174] NET: Registered PF_NETLINK/PF_ROUTE protocol family
[    0.306174] audit: initializing netlink subsys (disabled)
[    0.307287] thermal_sys: Registered thermal governor 'step_wise'
[    0.307291] thermal_sys: Registered thermal governor 'user_space'
[    0.308079] cpuidle: using governor menu
[    0.310243] PCI: Using configuration type 1 for base access
[    0.311094] audit: type=2000 audit(1661119071.482:1): state=initialized audit_enabled=0 res=1
[    0.314567] kprobes: kprobe jump-optimization is enabled. All kprobes are optimized if possible.
[    0.432654] HugeTLB: registered 2.00 MiB page size, pre-allocated 0 pages
[    0.433055] HugeTLB: 28 KiB vmemmap can be freed for a 2.00 MiB page
[    0.434192] ACPI: Added _OSI(Module Device)
[    0.435055] ACPI: Added _OSI(Processor Device)
[    0.436055] ACPI: Added _OSI(3.0 _SCP Extensions)
[    0.437056] ACPI: Added _OSI(Processor Aggregator Device)
[    0.438059] ACPI: Added _OSI(Linux-Dell-Video)
[    0.439056] ACPI: Added _OSI(Linux-Lenovo-NV-HDMI-Audio)
[    0.439932] ACPI: Added _OSI(Linux-HPI-Hybrid-Graphics)
[    0.440469] ACPI: 1 ACPI AML tables successfully acquired and loaded
[    0.442281] ACPI: Interpreter enabled
[    0.443113] ACPI: PM: (supports S0 S3 S4 S5)
[    0.444058] ACPI: Using IOAPIC for interrupt routing
[    0.445097] PCI: Using host bridge windows from ACPI; if necessary, use "pci=nocrs" and report a bug
[    0.446058] PCI: Using E820 reservations for host bridge windows
[    0.447187] ACPI: Enabled 2 GPEs in block 00 to 0F
[    0.450128] ACPI: PCI Root Bridge [PCI0] (domain 0000 [bus 00-ff])
[    0.451061] acpi PNP0A03:00: _OSC: OS supports [ASPM ClockPM Segments MSI HPX-Type3]
[    0.452056] acpi PNP0A03:00: _OSC: not requesting OS control; OS requires [ExtendedConfig ASPM ClockPM MSI]
[    0.453066] acpi PNP0A03:00: fail to add MMCONFIG information, can't access extended PCI configuration space under this bridge.
[    0.454182] PCI host bridge to bus 0000:00
[    0.455057] pci_bus 0000:00: root bus resource [io  0x0000-0x0cf7 window]
[    0.456058] pci_bus 0000:00: root bus resource [io  0x0d00-0xffff window]
[    0.457058] pci_bus 0000:00: root bus resource [mem 0x000a0000-0x000bffff window]
[    0.458056] pci_bus 0000:00: root bus resource [mem 0x80000000-0xfebfffff window]
[    0.459062] pci_bus 0000:00: root bus resource [mem 0x100000000-0x17fffffff window]
[    0.460057] pci_bus 0000:00: root bus resource [bus 00-ff]
[    0.461121] pci 0000:00:00.0: [8086:1237] type 00 class 0x060000
[    0.463491] pci 0000:00:01.0: [8086:7000] type 00 class 0x060100
[    0.465656] pci 0000:00:01.1: [8086:7010] type 00 class 0x010180
[    0.470064] pci 0000:00:01.1: reg 0x20: [io  0xc040-0xc04f]
[    0.473475] pci 0000:00:01.1: legacy IDE quirk: reg 0x10: [io  0x01f0-0x01f7]
[    0.474061] pci 0000:00:01.1: legacy IDE quirk: reg 0x14: [io  0x03f6]
[    0.475057] pci 0000:00:01.1: legacy IDE quirk: reg 0x18: [io  0x0170-0x0177]
[    0.476058] pci 0000:00:01.1: legacy IDE quirk: reg 0x1c: [io  0x0376]
[    0.477252] pci 0000:00:01.3: [8086:7113] type 00 class 0x068000
[    0.479350] pci 0000:00:01.3: quirk: [io  0x0600-0x063f] claimed by PIIX4 ACPI
[    0.480069] pci 0000:00:01.3: quirk: [io  0x0700-0x070f] claimed by PIIX4 SMB
[    0.482132] pci 0000:00:02.0: [1234:1111] type 00 class 0x030000
[    0.485090] pci 0000:00:02.0: reg 0x10: [mem 0xfd000000-0xfdffffff pref]
[    0.490102] pci 0000:00:02.0: reg 0x18: [mem 0xfebf0000-0xfebf0fff]
[    0.499093] pci 0000:00:02.0: reg 0x30: [mem 0xfebe0000-0xfebeffff pref]
[    0.500181] pci 0000:00:02.0: Video device with shadowed ROM at [mem 0x000c0000-0x000dffff]
[    0.502443] pci 0000:00:03.0: [8086:100e] type 00 class 0x020000
[    0.505062] pci 0000:00:03.0: reg 0x10: [mem 0xfebc0000-0xfebdffff]
[    0.508068] pci 0000:00:03.0: reg 0x14: [io  0xc000-0xc03f]
[    0.516064] pci 0000:00:03.0: reg 0x30: [mem 0xfeb80000-0xfebbffff pref]
[    0.519222] ACPI: PCI: Interrupt link LNKA configured for IRQ 10
[    0.520188] ACPI: PCI: Interrupt link LNKB configured for IRQ 10
[    0.521176] ACPI: PCI: Interrupt link LNKC configured for IRQ 11
[    0.522209] ACPI: PCI: Interrupt link LNKD configured for IRQ 11
[    0.524071] ACPI: PCI: Interrupt link LNKS configured for IRQ 9
[    0.525344] iommu: Default domain type: Translated 
[    0.526055] iommu: DMA domain TLB invalidation policy: lazy mode 
[    0.527189] SCSI subsystem initialized
[    0.528371] ACPI: bus type USB registered
[    0.530094] usbcore: registered new interface driver usbfs
[    0.531096] usbcore: registered new interface driver hub
[    0.532072] usbcore: registered new device driver usb
[    0.533110] pps_core: LinuxPPS API ver. 1 registered
[    0.534056] pps_core: Software ver. 5.3.6 - Copyright 2005-2007 Rodolfo Giometti <giometti@linux.it>
[    0.535070] PTP clock support registered
[    0.536224] Advanced Linux Sound Architecture Driver Initialized.
[    0.537290] NetLabel: Initializing
[    0.538055] NetLabel:  domain hash size = 128
[    0.539055] NetLabel:  protocols = UNLABELED CIPSOv4 CALIPSO
[    0.540085] NetLabel:  unlabeled traffic allowed by default
[    0.541156] PCI: Using ACPI for IRQ routing
[    0.542154] pci 0000:00:02.0: vgaarb: setting as boot VGA device
[    0.543053] pci 0000:00:02.0: vgaarb: bridge control possible
[    0.543053] pci 0000:00:02.0: vgaarb: VGA device added: decodes=io+mem,owns=io+mem,locks=none
[    0.543059] vgaarb: loaded
[    0.544133] hpet: 3 channels of 0 reserved for per-cpu timers
[    0.545079] hpet0: at MMIO 0xfed00000, IRQs 2, 8, 0
[    0.546055] hpet0: 3 comparators, 64-bit 100.000000 MHz counter
[    0.557857] clocksource: Switched to clocksource kvm-clock
[    0.558273] VFS: Disk quotas dquot_6.6.0
[    0.558992] VFS: Dquot-cache hash table entries: 512 (order 0, 4096 bytes)
[    0.563239] pnp: PnP ACPI init
[    0.564265] pnp: PnP ACPI: found 6 devices
[    0.575265] clocksource: acpi_pm: mask: 0xffffff max_cycles: 0xffffff, max_idle_ns: 2085701024 ns
[    0.579371] NET: Registered PF_INET protocol family
[    0.580391] IP idents hash table entries: 32768 (order: 6, 262144 bytes, linear)
[    0.582341] tcp_listen_portaddr_hash hash table entries: 1024 (order: 2, 16384 bytes, linear)
[    0.584007] Table-perturb hash table entries: 65536 (order: 6, 262144 bytes, linear)
[    0.585520] TCP established hash table entries: 16384 (order: 5, 131072 bytes, linear)
[    0.586852] TCP bind hash table entries: 16384 (order: 6, 262144 bytes, linear)
[    0.588109] TCP: Hash tables configured (established 16384 bind 16384)
[    0.589392] UDP hash table entries: 1024 (order: 3, 32768 bytes, linear)
[    0.590521] UDP-Lite hash table entries: 1024 (order: 3, 32768 bytes, linear)
[    0.591768] NET: Registered PF_UNIX/PF_LOCAL protocol family
[    0.592992] RPC: Registered named UNIX socket transport module.
[    0.593979] RPC: Registered udp transport module.
[    0.595358] RPC: Registered tcp transport module.
[    0.596829] RPC: Registered tcp NFSv4.1 backchannel transport module.
[    0.598628] pci_bus 0000:00: resource 4 [io  0x0000-0x0cf7 window]
[    0.599808] pci_bus 0000:00: resource 5 [io  0x0d00-0xffff window]
[    0.601046] pci_bus 0000:00: resource 6 [mem 0x000a0000-0x000bffff window]
[    0.602282] pci_bus 0000:00: resource 7 [mem 0x80000000-0xfebfffff window]
[    0.603555] pci_bus 0000:00: resource 8 [mem 0x100000000-0x17fffffff window]
[    0.604904] pci 0000:00:01.0: PIIX3: Enabling Passive Release
[    0.605981] pci 0000:00:00.0: Limiting direct PCI/PCI transfers
[    0.607154] PCI: CLS 0 bytes, default 64
[    0.608016] clocksource: tsc: mask: 0xffffffffffffffff max_cycles: 0x23f44f92df3, max_idle_ns: 440795329494 ns
[    0.611614] Initialise system trusted keyrings
[    0.612589] workingset: timestamp_bits=56 max_order=19 bucket_order=0
[    0.615252] NFS: Registering the id_resolver key type
[    0.616204] Key type id_resolver registered
[    0.616910] Key type id_legacy registered
[    0.617656] 9p: Installing v9fs 9p2000 file system support
[    0.628673] Key type asymmetric registered
[    0.629420] Asymmetric key parser 'x509' registered
[    0.630268] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 251)
[    0.631529] io scheduler mq-deadline registered
[    0.632323] io scheduler kyber registered
[    0.633144] input: Power Button as /devices/LNXSYSTM:00/LNXPWRBN:00/input/input0
[    0.634534] ACPI: button: Power Button [PWRF]
[    0.635995] Serial: 8250/16550 driver, 4 ports, IRQ sharing enabled
[    0.637555] 00:04: ttyS0 at I/O 0x3f8 (irq = 4, base_baud = 115200) is a 16550A
[    0.639632] Non-volatile memory driver v1.3
[    0.640450] Linux agpgart interface v0.103
[    0.641263] ACPI: bus type drm_connector registered
[    0.644235] loop: module loaded
[    0.646205] scsi host0: ata_piix
[    0.647137] scsi host1: ata_piix
[    0.647791] ata1: PATA max MWDMA2 cmd 0x1f0 ctl 0x3f6 bmdma 0xc040 irq 14
[    0.649048] ata2: PATA max MWDMA2 cmd 0x170 ctl 0x376 bmdma 0xc048 irq 15
[    0.650931] e100: Intel(R) PRO/100 Network Driver
[    0.651737] e100: Copyright(c) 1999-2006 Intel Corporation
[    0.652693] e1000: Intel(R) PRO/1000 Network Driver
[    0.653523] e1000: Copyright (c) 1999-2006 Intel Corporation.
[    0.669336] ACPI: \_SB_.LNKC: Enabled at IRQ 11
[    0.809958] ata2: found unknown device (class 0)
[    0.811384] ata1: found unknown device (class 0)
[    0.812888] ata1.00: ATA-7: QEMU HARDDISK, 2.5+, max UDMA/100
[    0.814149] ata1.00: 32768 sectors, multi 16: LBA48 
[    0.815612] ata2.00: ATAPI: QEMU DVD-ROM, 2.5+, max UDMA/100
[    0.817582] scsi 0:0:0:0: Direct-Access     ATA      QEMU HARDDISK    2.5+ PQ: 0 ANSI: 5
[    0.819205] scsi 0:0:0:0: Attached scsi generic sg0 type 0
[    0.820519] scsi 1:0:0:0: CD-ROM            QEMU     QEMU DVD-ROM     2.5+ PQ: 0 ANSI: 5
[    0.823147] sd 0:0:0:0: [sda] 32768 512-byte logical blocks: (16.8 MB/16.0 MiB)
[    0.828412] sd 0:0:0:0: [sda] Write Protect is off
[    0.829300] sd 0:0:0:0: [sda] Write cache: enabled, read cache: enabled, doesn't support DPO or FUA
[    0.831045] sd 0:0:0:0: [sda] Preferred minimum I/O size 512 bytes
[    0.836850] sd 0:0:0:0: [sda] Attached SCSI disk
[    0.867870] sr 1:0:0:0: [sr0] scsi3-mmc drive: 4x/4x cd/rw xa/form2 tray
[    0.869071] cdrom: Uniform CD-ROM driver Revision: 3.20
[    0.895971] sr 1:0:0:0: Attached scsi generic sg1 type 5
[    1.031077] e1000 0000:00:03.0 eth0: (PCI:33MHz:32-bit) 52:54:00:12:34:56
[    1.032349] e1000 0000:00:03.0 eth0: Intel(R) PRO/1000 Network Connection
[    1.033618] e1000e: Intel(R) PRO/1000 Network Driver
[    1.034483] e1000e: Copyright(c) 1999 - 2015 Intel Corporation.
[    1.035521] sky2: driver version 1.30
[    1.036244] ehci_hcd: USB 2.0 'Enhanced' Host Controller (EHCI) Driver
[    1.037353] ehci-pci: EHCI PCI platform driver
[    1.038117] ohci_hcd: USB 1.1 'Open' Host Controller (OHCI) Driver
[    1.039422] ohci-pci: OHCI PCI platform driver
[    1.040254] uhci_hcd: USB Universal Host Controller Interface driver
[    1.041376] usbcore: registered new interface driver usblp
[    1.042443] usbcore: registered new interface driver usb-storage
[    1.043612] i8042: PNP: PS/2 Controller [PNP0303:KBD,PNP0f13:MOU] at 0x60,0x64 irq 1,12
[    1.045778] serio: i8042 KBD port at 0x60,0x64 irq 1
[    1.046783] serio: i8042 AUX port at 0x60,0x64 irq 12
[    1.048027] input: AT Translated Set 2 keyboard as /devices/platform/i8042/serio0/input/input1
[    1.051456] rtc_cmos 00:05: RTC can wake from S4
[    1.052848] rtc_cmos 00:05: registered as rtc0
[    1.054368] rtc_cmos 00:05: alarms up to one day, y3k, 242 bytes nvram, hpet irqs
[    1.056532] device-mapper: ioctl: 4.47.0-ioctl (2022-07-28) initialised: dm-devel@redhat.com
[    1.058051] intel_pstate: CPU model not supported
[    1.059024] hid: raw HID events driver (C) Jiri Kosina
[    1.060317] usbcore: registered new interface driver usbhid
[    1.061334] usbhid: USB HID core driver
[    1.062946] Initializing XFRM netlink socket
[    1.064123] NET: Registered PF_INET6 protocol family
[    1.065554] Segment Routing with IPv6
[    1.066253] In-situ OAM (IOAM) with IPv6
[    1.066966] sit: IPv6, IPv4 and MPLS over IPv4 tunneling driver
[    1.068213] NET: Registered PF_PACKET protocol family
[    1.069159] 9pnet: Installing 9P2000 support
[    1.069892] Key type dns_resolver registered
[    1.070782] IPI shorthand broadcast: enabled
[    1.071540] sched_clock: Marking stable (1055120084, 16385835)->(1169965497, -98459578)
[    1.072983] registered taskstats version 1
[    1.074076] Loading compiled-in X.509 certificates
[    1.075269] cryptomgr_test (43) used greatest stack depth: 15528 bytes left
[    1.077407] PM:   Magic number: 2:263:1006
[    1.078207] printk: console [netcon0] enabled
[    1.079012] netconsole: network logging started
[    1.080005] cfg80211: Loading compiled-in X.509 certificates for regulatory database
[    1.081599] kworker/u2:2 (65) used greatest stack depth: 14592 bytes left
[    1.083190] cfg80211: Loaded X.509 cert 'sforshee: 00b28ddf47aef9cea7'
[    1.084547] platform regulatory.0: Direct firmware load for regulatory.db failed with error -2
[    1.086139] ALSA device list:
[    1.086764]   No soundcards found.
[    1.087358] cfg80211: failed to load regulatory.db
[    1.676055] input: ImExPS/2 Generic Explorer Mouse as /devices/platform/i8042/serio1/input/input3
[    1.677928] md: Waiting for all devices to be available before autodetect
[    1.679084] md: If you don't use raid, use raid=noautodetect
[    1.679933] md: Autodetecting RAID arrays.
[    1.680652] md: autorun ...
[    1.681491] md: ... autorun DONE.
[    1.688030] EXT4-fs (sda): INFO: recovery required on readonly filesystem
[    1.689700] EXT4-fs (sda): write access will be enabled during recovery
[    1.705661] EXT4-fs (sda): recovery complete
[    1.709119] EXT4-fs (sda): mounted filesystem with ordered data mode. Quota mode: none.
[    1.710197] VFS: Mounted root (ext4 filesystem) readonly on device 8:0.
[    1.711118] devtmpfs: mounted
[    1.712174] Freeing unused kernel image (initmem) memory: 1528K
[    1.712977] Write protecting the kernel read-only data: 24576k
[    1.714781] Freeing unused kernel image (text/rodata gap) memory: 2032K
[    1.715789] Freeing unused kernel image (rodata/data gap) memory: 980K
[    1.716699] Run /init as init process
[    1.719269] process '/bin/busybox' started with executable stack
[    1.731218] mount (72) used greatest stack depth: 14328 bytes left
[    1.735361] cut (74) used greatest stack depth: 13832 bytes left

Boot took 1.71 seconds


  _____ _____ _  __          _____ _    _ _    ___   __
 |  __ \_   _| |/ /    /\   / ____| |  | | |  | \ \ / /
 | |__) || | | ' /    /  \ | |    | |__| | |  | |\ V / 
 |  ___/ | | |  <    / /\ \| |    |  __  | |  | | > <  
 | |    _| |_| . \  / ____ \ |____| |  | | |__| |/ . \ 
 |_|   |_____|_|\_\/_/    \_\_____|_|  |_|\____//_/ \_                                                       
                                                       

Welcome to pikachux


/bin/sh: can't access tty; job control turned off
~ # 