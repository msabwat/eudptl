bits 32
section .multiboot               ;according to multiboot spec
align 4
        dd 0x1BADB002            ;set magic number for
                                 ;bootloader
        dd ( 1 << 0 ) | (1 << 1) ;set flags ; (Align + meminfo + vidinfo)
        dd - (0x1BADB002 + (( 1 << 0 ) | (1 << 1) ))

section .text
global start
global outb
global inb
global _asm_fb_move_cursor
extern goose_entry               ;defined in the C file
global _asm_gdt_flush
FB_HIGH_BYTE equ 14
FB_LOW_BYTE equ 15
FB_CMD equ 0x3D4
FB_DATA equ 0x3D5

_asm_fb_move_cursor:
	mov bx, [esp + 4]
	mov al, FB_HIGH_BYTE 
	out FB_CMD, al

	mov al, bh
    out 0x3D5, al
	
	mov al, FB_LOW_BYTE
    out 0x3D4, al

	mov al, bl 
   	out 0x3D5, al
	ret

_asm_gdt_flush:
	mov eax, [esp + 4]
	lgdt [eax]

	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	jmp 0x08:.flush
.flush:
	ret

outb:
	mov al, [esp + 8]
	mov dx, [esp + 4]
	out dx, al
	ret

inb:
	mov dx, [esp + 4]
	in al, dx
	ret

start:
        cli                      ;block interrupts
        mov esp, stack_space     ;set stack pointer
        ;; TODO: get the arguments from the bootloader
	push eax		; magic from the bootloader (grub)
	push ebx		; push struct multiboot_info
	call goose_entry
	hlt                      ;halt the CPU

section .bss
resb 8192                        ;8KB for stack
stack_space:
