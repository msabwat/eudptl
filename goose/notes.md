# Outils pour démarrer le kernel

## U-Boot

I have a preference for uboot. From what I understand it covers more stages
(stage one boot instead of 3 for grub). I am in love with the command line. And
I am fed up with grub. However, I am starting this project for school, grub is
a requirement.

```
# uboot setup (dans un clone de U-Boot)
make qemu-x86_defconfig # tu as une ribambelle d'architectures
make all
qemu-system-i386 -nographic -bios u-boot.rom
```

Un setup sécurisé pourrait être fait en se basant sur ce lien :
https://connect.ed-diamond.com/GNU-Linux-Magazine/glmf-221/u-boot-a-la-decouverte-du-demarrage-verifie

TODO: à partir de Novembre

## Grub

Avec ce fichier de configuration:

```
set timeout=3
menuentry "Goose" {
	multiboot <path to kernel bin>
	boot
}
```

ce fichier doit être placé dans un dossier <image>/boot/grub/grub.cfg, pour
que la commande grub-mkrescue avec le dossier <image> en input.

# Amorçage d'un noyau pour un cpu x86

Pour démarrer le noyau, il faut pouvoir communiquer au bootloader :

- le header multiboot:

Il y a une spécification qui permet de dire à un bootloader comment il doit
charger un noyau. Le binaire est un binaire comme un autre, qui se distingue
par un magic 0x1BADB002, une taille de 8Kb au début et un alignement à 4 bytes.

- la taille de la stack:

l'esp n'est pas défini dans le standard multiboot, on peut choisir la taille
de la stack. Dans la section .bss on initialise la taille de la stack avec resb
et on définit les labels stack_bottom et stack_top (en dessous).

On peut le faire en C ou en asm. Il est demandé dans le sujet d'appeler le point
d'entrée en asm.

- la fonction à exécuter quand on quitte le bootloader:

Cette fonction doit pointer vers le point d'entrée de notre noyau. C'est un
symbole externe, qui sera retrouvé grâce au linker.

Le CPU ne peut pas rien faire, soit il est à l'écoute des interruptions, soit
il est dans un état où il ne peut plus les traiter. Dans ce cas, c'est une
boucle infinie.

L'instruction cli va Clear Interrupts, Enable EFLAGS.

EFLAGS est un registre de 32 bits (successeur de FLAGS pour 16, ancêtre de
RFLAGS pour 64), il permet de connaitre et contrôller l'état du processeur.
( et l'eau ça mouille). En faisant un petit effort, c'est un registre qui
permet de refléter les résultats d'une opération en cours (carry, pour le
reste d'une opération). En l'occurence c'est ce qui permettra à notre noyau
d'ignorer les interruptions. (merci Wikipedia).

Dans la documentation d'OSDev (BareBones), il y a une instruction utile qui
permet de s'assurer que notre cpu ne sera pas mis dans un état autre que celui
qu'on souhaite :

```
.hang: 	hlt ; pour attendre l'interruption qui n'arrivera jamais.
	jmp .hang ; au cas où une interruption non masquable (nmi, erreur de
		  ; parité, corruption mémoire etc...) serait appelée.
```

# Que peut on faire en real mode?

Le real mode est le mode par défaut dans lequel le cpu x86 se trouve,
la mémoire n'y es pas segmentée ou paginée. Aussi, l'adressage **par défaut**
est seulement de 16 bits, pas de mémoire virtuelle, et 1MB de mémoire vive.
Il est cependant intéressant pour installer les pilotes matériels comme
l'écran et le clavier, et gérer les interruptions. Les accès mémoire sont
plus rapides aussi, puisqu'il n'y a pas besoin de chercher dans des tables
de descriptors.

Pour KFS1, je vais utiliser les BIOS Functions (donc les interruptions)
pour afficher un message de bienvenue. Avant ce message, je voudrais
afficher des informations sur certains registres. Aussi, afficher
des options passées en lignes de commande à partir du bootloader.
Une fois dans entry.c, le point d'entrée du kernel, il y aura des fonctions
de test de la libft (fonctions mem, string et liste), avec possiblement des
asserts et un printk() qui affiche le temps.

# Structurer la mémoire pour écrire une api pour le noyau
 (partie de la libft à ajouter, avec un header)

# printk implementation

# GDT implementation
