NAME 		= goose

GOOSE_FLAGS	= -Wall -Werror -Wextra -m32 -fno-builtin -fno-exceptions \
		-fno-stack-protector -nostdlib -nodefaultlibs

L_FLAGS		= -m elf_i386 -T linker.ld

NASM_FLAGS	= -f elf32

MACHINE_OPTIONS = -enable-kvm -m 1024 -cdrom $(NAME).iso

CC		= gcc

INC		= -I.

IMG_PATH	= boot/grub

UTILS_SRC_NAME	= strings.c serial.c framebuffer.c \
		descriptor_tables.c list.c mem.c print.c

UTILS_OBJ_NAME	= $(UTILS_SRC_NAME:.c=.o)

UTILS_SRC_PATH	= ft

OBJ_PATH	= .obj

UTILS_SRC	= $(addprefix $(UTILS_SRC_PATH)/,$(UTILS_SRC_NAME))

UTILS_OBJS	= $(addprefix $(OBJ_PATH)/,$(UTILS_OBJ_NAME))

UID		= $(shell id -u ${USER})

GID		= $(shell id -g ${USER})

.PHONY:  all clean re

all: makedirs image

makedirs:
	@mkdir -p .obj
	@mkdir -p .iso/$(IMG_PATH)
	docker build . -t complex-for-nothing
run: image
	qemu-system-i386 $(MACHINE_OPTIONS) -nographic
run-gfx: image
	qemu-system-i386 $(MACHINE_OPTIONS)
debug-run: GOOSE_FLAGS += -g
debug-run: image
	qemu-system-i386 $(MACHINE_OPTIONS) -S -s

#gdb kernel -ex 'target remote localhost:1234' \
#	-ex 'break *goose_entry' \
#	-ex 'continue'
#	list (pour voir le code source avant de mettre un bp)

image: check
	# never do that if you want your image to boot
	# on another machine
	# grub-mkrescue -o $(NAME).iso .iso
	docker run -it -d --rm -v $(PWD):/tmp \
	--name work complex-for-nothing
	docker wait work
check: kernel makedirs
	grub-file --is-x86-multiboot kernel
	cp kernel .iso/boot/
	cp grub.cfg .iso/$(IMG_PATH)

$(OBJ_PATH)/%.o: $(UTILS_SRC_PATH)/%.c
	$(CC) $(GOOSE_FLAGS) $(INC) -c $< -o $@

$(OBJ_PATH)/boot.o: boot.asm
	nasm $(NASM_FLAGS) boot.asm -o $(OBJ_PATH)/boot.o

kernel: makedirs linker.ld $(UTILS_OBJS) $(OBJ_PATH)/boot.o
	$(CC) $(GOOSE_FLAGS) $(INC) -c entry.c -o $(OBJ_PATH)/entry.o
	ld $(L_FLAGS) -o kernel $(OBJ_PATH)/boot.o $(OBJ_PATH)/entry.o \
		$(UTILS_OBJS)

fclean: clean
	rm -fr goose.iso
clean:
	rm -fr .obj
	rm -fr .iso
	rm -fr kernel
re: clean all
