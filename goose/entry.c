#include "goose.h"
/* 
 * TODO: only include stdlib, NOT modules
 * their functionalities should be exposed
 * through the kapi
 */
#include "ft/ft.h"
#include "multiboot.h"

/* the only global allowed outside modules */
sys_t sys;

static void init_sys(sys_t *sys, const char options[]) {
	if (ft_strncmp(options, "com1", 4) == 0) {
		sys->out = console_serial_com1;
		
	}
	else if (ft_strncmp(options, "vga", 3) == 0) {
		sys->out = console_vga;
	}
	else {
		sys->out = console_all;
	}
	init_descriptor_tables(sys);
}

void goose_entry(multiboot_info_t *info, unsigned long magic) {
	// TODO: multiboot 1 ou 2?
	/*
	if (magic != 0x1BADB002)
		return ;
	*/
	(void)magic;
	const char *cmd = (char *)info->cmdline;
	init_sys(&sys, cmd);

	if ((sys.out == console_serial_com1) && (sys.out == console_all))
		kprint("\n");

	kprint("Successfuly booted Goose!\n\n");
	kprint_stack();
	kprint("\n\nBye! ne m'oublie pas!\n");
	// write_string("Press enter to continue...");
	// start the timer (honk honk every 5s)
	return ;
}
