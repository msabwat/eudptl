#include "ft.h"
#include "goose.h"

static void gdt_set_entry(sys_t *sys, int32_t entry, uint32_t base,
		uint32_t limit, uint8_t access, uint8_t granularity) {
	sys->gdt[entry].limit_low = (limit & 0xFFFF);
	sys->gdt[entry].base_low = (base & 0xFFFF);
	sys->gdt[entry].base_middle = (base >> 16) & 0xFF;
	sys->gdt[entry].base_high = (base >> 24) & 0xFF;

	sys->gdt[entry].granularity = (limit >> 16) & 0x0F;
	sys->gdt[entry].granularity |= granularity & 0xF0;

	sys->gdt[entry].access = access;
}

static void init_gdt(sys_t *sys) {
	/* 42 subject requires putting the gdt at this address */
	sys->p_gdt = (gdt_ptr_t *)0x00000800;
	sys->p_gdt->limit = (sizeof(gdt_entry_t) * 7 ) - 1;
	sys->p_gdt->base  = ((uint32_t)&(sys->gdt));

	gdt_set_entry(sys, 0, 0, 0, 0, 0);

	gdt_set_entry(sys, 1, 0, 0xFFFF, (uint8_t)(GDT_CODE_RING0), 0xCF);
	gdt_set_entry(sys, 2, 0, 0xFFFF, (uint8_t)(GDT_DATA_RING0), 0xCF);
	gdt_set_entry(sys, 3, 0, 0x4242, (uint8_t)(GDT_STACK_RING0), 0xCF);
	gdt_set_entry(sys, 4, 0, 0xFFFF, (uint8_t)(GDT_CODE_URING), 0xCF);
	gdt_set_entry(sys, 5, 0, 0xFFFF, (uint8_t)(GDT_DATA_URING), 0xCF);
	gdt_set_entry(sys, 6, 0, 0x4242, (uint8_t)(GDT_STACK_URING), 0xCF);

	_asm_gdt_flush( (uint32_t)sys->p_gdt );
}

void init_descriptor_tables (sys_t *sys) {
	init_gdt(sys);
}

