#ifndef FT_H
# define FT_H
#include "goose.h"

int ft_isalpha(int c);
int ft_isdigit(int c);
size_t ft_strlen(const char *str);
void ft_swap(int *a, int *b);
int ft_toupper(int c);
int ft_tolower(int c);
char *ft_strstr(const char *haystack, const char *needle);
char  *ft_strrchr(const char *s, int c);
char *ft_strnstr(const char *haystack, const char *needle, size_t len);
int ft_strnequ(char const *s1, char const *s2, size_t n);
char *ft_strncpy(char *dst, const char *src, size_t len);
int ft_strncmp(const char *s1, const char *s2, size_t n);
char *ft_strncat(char *s1, const char *s2, size_t n);
size_t ft_strlen(const char *s);
size_t ft_strlcat(char *dst, const char *src, size_t size);
void ft_striteri(char *s, void (*f) (unsigned int, char *));
void ft_striter(char *s, void (*f) (char *));
int ft_strequ(char const *s1, char const *s2);
char *ft_strcpy(char *dst, const char *src);
int ft_strcmp(const char *s1, const char *s2);
void ft_strclr(char *s);
char *ft_strchr(const char *s, int c);
char *ft_strcat(char *s1, const char *s2);
int ft_isprint(int c);
int ft_isdigit(int c);
int ft_isascii(int c);
int ft_isalpha(int c);
int ft_isalnum(int c);
void ft_bzero(void *s, size_t n);
int ft_atoi(const char *str);
void itoa_hex(char *res, int num);

#define VGA_PTR 0xB8000
#define COLUMNS 80
#define LINES 25

#define COLOR_BLACK		0
#define COLOR_BLUE		1
#define COLOR_GREEN		2
#define COLOR_CYAN		3
#define COLOR_RED		4
#define COLOR_MAGENTA		5
#define COLOR_BROWN		6
#define COLOR_LIGHT_GREY	7
#define COLOR_DARK_GREY		8
#define COLOR_LIGHT_BLUE	9
#define COLOR_LIGHT_GREEN	10
#define COLOR_LIGHT_CYAN	11
#define COLOR_LIGHT_RED		12
#define COLOR_LIGHT_MAGENTA	13
#define COLOR_LIGHT_BROWN	14
#define COLOR_WHITE		15

typedef struct framebuffer {
	char *p_vga;
	int pos_x;
	int pos_y;
} fb;

// I/O ports
#define SERIAL_COM1_BASE 0x3F8
#define SERIAL_DATA_PORT(base) (base)
#define SERIAL_FIFO_COMMAND_PORT(base) (base + 2)
#define SERIAL_LINE_COMMAND_PORT(base) (base + 3)
#define SERIAL_MODEM_COMMAND_PORT(base) (base + 4)
#define SERIAL_LINE_STATUS_PORT(base) (base + 5)
#define SERIAL_LINE_ENABLE_DLAB 0x80

enum BaudRate { Baud_115200 = 1, Baud_57600, Baud_19200, Baud_9600 };

typedef struct serial {
	// TODO: handle multiple serial ports
	enum BaudRate rate;
	unsigned short port;
} com;

unsigned char inb(unsigned short port);
void outb(unsigned short port, unsigned char data);

void kprint(const char *str);
void kprint_hex(uint32_t addr);
void kprint_stack();

void serial_write(unsigned short port, const char *str, unsigned int size);

void fb_write(unsigned int, char, unsigned char, unsigned char);
void fb_write_str(const char *, unsigned char, unsigned char);
void fb_clear();
extern void _asm_fb_move_cursor(unsigned short);
#define fb_move_cursor _asm_fb_move_cursor

#endif
