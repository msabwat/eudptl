#include "ft.h"

static int      toskip(char c)
{
        if ((c <= 32 && c != 27 && c != '\200') && c != '\0')
                return (1);
        return (0);
}

int                     ft_atoi(const char *str)
{
        int     cnt;
        int     number;
        int     s;

        s = 0;
        number = 0;
        cnt = 0;
        while (toskip(str[cnt]))
                cnt++;
        if (str[cnt] == '+' && str[cnt + 1] >= '0' && str[cnt + 1] <= '9')
                cnt++;
        if (str[cnt] == '-' && str[cnt + 1] >= '0' && str[cnt + 1] <= '9' && s == 0)
        {
                cnt++;
                s = 1;
        }
        while ((str[cnt] >= '0' && str[cnt] <= '9') && str[cnt] != '\0')
        {
                if (s == 0)
                        number = 10 * number + str[cnt] - 48;
                else
                        number = 10 * number - str[cnt] + 48;
                cnt++;
        }
        return (number);
}

void	ft_bzero(void *s, size_t n)
{
	char	*a;

	if (n != 0)
	{
		a = (char *)s;
		while (n)
		{
			*a = '\0';
			a++;
			n--;
		}
	}
}

int	ft_isalnum(int c)
{
	if (ft_isalpha(c) || ft_isdigit(c))
		return (1);
	return (0);
}

int	ft_isalpha(int c)
{
	if ((c >= 65 && c <= 90) || (c >= 97 && c <= 122))
		return (1);
	return (0);
}

int	ft_isascii(int c)
{
	if (c >= 0 && c <= 127)
		return (1);
	return (0);
}

int	ft_isdigit(int c)
{
	if (c >= '0' && c <= '9')
		return (1);
	return (0);
}

int	ft_isprint(int c)
{
	if (c > 32 && c <= 126)
		return (1);
	return (0);
}
/*
static size_t	ft_isword(char const *str, int i, char delim)
{
	if (i == 0)
		return (0);
	if ((str[i] == delim || str[i] == '\0')
		&& (str[i - 1] != delim && str[i - 1] != '\0'))
		return (1);
	return (0);
}

static	int	count_nb(int nb)
{
	int	count;

	count = 1;
	if (nb < 0)
	{
		count++;
		nb = -nb;
	}
	while ((nb / 10) > 0)
	{
		nb = nb / 10;
		count++;
	}
	return (count);
}

static char	*isspecial(int num)
{
	if (num == 0)
		return (ft_strdup("0"));
	else if (num == -2147483648)
		return (ft_strdup("-2147483648"));
	return (NULL);
}

static char	*prepare_str(int n, int cnt)
{
	return (str);
}

static size_t	ft_cntdelimwords(char const *str, char d)
{
	int	j;
	int	cnt;

	cnt = 0;
	j = 0;
	while (str[j])
	{
		if (ft_isword(str, j, d))
			cnt++;
		j++;
	}
	if (ft_isword(str, j, d))
		cnt++;
	return (cnt);
}

char		*ft_itoa(int n)
{
	int		cnt;
	int		nbr;
	char	str[cnt + 1];

	if (n < 0)
		str[0] = '-';
	str[cnt] = '\0';

	nbr = n;
	cnt = count_nb(n);
	str = isspecial(nbr);
	if (str)
		return (str);
	str = prepare_str(n, cnt);
	if (n < 0)
		n = -n;
	while (cnt > 0)
	{
		if (cnt - 1 == 0 && nbr < 0)
			return (str);
		str[cnt - 1] = (char)((n % 10) + 48);
		n = n / 10;
		cnt--;
	}
	return (str);
}
*/

char	*ft_strcat(char *s1, const char *s2)
{
	char	*s;

	s = s1;
	while (*s)
		s++;
	while (*s2)
	{
		*s = *s2;
		s++;
		s2++;
	}
	*s = '\0';
	return (s1);
}

char	*ft_strchr(const char *s, int c)
{
	size_t			i;
	unsigned char	ch;

	i = 0;
	ch = (const char)c;
	if (!s)
		return (NULL);
	while (s[i] != ch && s[i] != '\0')
		i++;
	if (s[i] == ch)
		return ((char *)&s[i]);
	return (NULL);
}

void	ft_strclr(char *s)
{
	if (!s)
		return ;
	while (*s)
	{
		*s = '\0';
		s++;
	}
}

int	ft_strcmp(const char *s1, const char *s2)
{
	while (*s1 && *s1 == *s2)
	{
		s1++;
		s2++;
	}
	return (*(unsigned char *)s1 - *(unsigned char *)s2);
}

char	*ft_strcpy(char *dst, const char *src)
{
	int	i;

	i = 0;
	while (src[i])
	{
		dst[i] = src[i];
		i++;
	}
	dst[i] = '\0';
	return (dst);
}
/*
void	ft_strdel(char **as)
{
	if (!as)
		return ;
	free(*as);
	*as = NULL;
}
char	*ft_strdup(const char *s1)
{
	char	*cpy;
	size_t	i;
	size_t	len;

	len = ft_strlen(s1);
	i = 0;
	cpy = malloc((sizeof(char) * len) + 1);
	if (!cpy)
		return (NULL);
	while (i < len)
	{
		cpy[i] = s1[i];
		i++;
	}
	cpy[i] = '\0';
	return (cpy);
}
*/
int	ft_strequ(char const *s1, char const *s2)
{
	if (!s1 || !s2)
		return (0);
	if (ft_strcmp(s1, s2) == 0)
		return (1);
	return (0);
}

void	ft_striter(char *s, void (*f) (char *))
{
	if (!s || !f)
		return ;
	while (*s)
	{
		f(s);
		s++;
	}
}

void	ft_striteri(char *s, void (*f) (unsigned int, char *))
{
	unsigned int	i;

	i = 0;
	if (!s || !f)
		return ;
	while (*s)
	{
		f(i, s);
		s++;
		i++;
	}
}
/*
char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*str;

	if (!s1 || !s2)
		return (NULL);
	str = (char *)malloc(sizeof(char) * (ft_strlen(s1) + ft_strlen(s2) + 1));
	if (!str)
		return (NULL);
	ft_strcpy(str, s1);
	ft_strcat(str, s2);
	return (str);
}
*/

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	i;
	size_t	j;
	size_t	dstl;
	size_t	srcl;

	i = 0;
	j = 0;
	dstl = ft_strlen(dst);
	srcl = ft_strlen(src);
	if (size <= dstl)
		return (srcl + size);
	while ((dst[i] != '\0') && i < (size - 1))
		i++;
	while (src[j] && i < (size - 1))
	{
		dst[i] = src[j];
		i++;
		j++;
	}
	dst[i] = '\0';
	return (dstl + srcl);
}

size_t	ft_strlen(const char *s)
{
	size_t	i;

	i = 0;
	while (*s)
	{
		s++;
		i++;
	}
	return (i);
}
/*
char	*ft_strmap(char const *s, char (*f) (char))
{
	char	*str;
	size_t	cnt;

	cnt = 0;
	if (!s)
		return (NULL);
	str = (char *)malloc(sizeof(char) * (ft_strlen(s) + 1));
	if (!str)
		return (NULL);
	while (s[cnt])
	{
		str[cnt] = f(s[cnt]);
		cnt++;
	}
	str[cnt] = '\0';
	return (str);
}

char	*ft_strmapi(char const *s, char (*f) (unsigned int, char))
{
	char			*str;
	unsigned int	cnt;

	cnt = 0;
	if (!s)
		return (NULL);
	str = (char *)malloc(sizeof(char) * (ft_strlen(s) + 1));
	if (!str)
		return (NULL);
	while (s[cnt])
	{
		str[cnt] = f(cnt, s[cnt]);
		cnt++;
	}
	str[cnt] = '\0';
	return (str);
}
*/

char	*ft_strncat(char *s1, const char *s2, size_t n)
{
	size_t	i;
	size_t	j;

	i = 0;
	j = 0;
	while (s1[i])
		i++;
	while (s2[j] && j < n)
	{
		s1[i + j] = s2[j];
		j++;
	}
	s1[i + j] = '\0';
	return (s1);
}

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t	i;

	i = 0;
	while ((s1[i] || s2[i]) && i < n)
	{
		if (s1[i] != s2[i])
			return (((unsigned char)s1[i] - (unsigned char)s2[i]));
		i++;
	}
	return (0);
}

char	*ft_strncpy(char *dst, const char *src, size_t len)
{
	size_t	i;

	i = 0;
	while (i < len && src[i])
	{
		dst[i] = src[i];
		i++;
	}
	while (i < len)
	{
		dst[i] = '\0';
		i++;
	}
	return (dst);
}

int	ft_strnequ(char const *s1, char const *s2, size_t n)
{
	if (!s1 || !s2)
		return (0);
	if (ft_strncmp(s1, s2, n) == 0)
		return (1);
	return (0);
}
/*
char	*ft_strnew(size_t size)
{
	char	*p;
	size_t	i;

	i = 0;
	p = (char *)malloc(sizeof(char) * (size + 1));
	if (!p)
		return (NULL);
	while (i < size)
	{
		p[i] = '\0';
		i++;
	}
	p[i] = '\0';
	return (p);
}
*/

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t	l;

	if (*needle == '\0')
		return ((char *)haystack);
	l = ft_strlen(needle);
	while (*haystack && len >= l)
	{
		if (ft_strncmp(haystack, needle, l) == 0)
			return ((char *)haystack);
		haystack++;
		len--;
	}
	return (NULL);
}

char	*ft_strrchr(const char *s, int c)
{
	size_t			i;
	unsigned char	ch;
	char			*temp;

	i = 0;
	temp = NULL;
	ch = (const char)c;
	while (s[i])
	{
		if (s[i] == ch)
			temp = (char *)&s[i];
		i++;
	}
	if (s[i] == ch)
		temp = (char *)&s[i];
	return (temp);
}
/*
static char	*get_prevword(char *s, size_t index, char delim)
{
	char	*temp;
	int		x;
	int		cnt;

	x = index - 1;
	cnt = 0;
	while (x >= 0 && s[x] != delim)
		x--;
	x++;
	temp = (char *)malloc(sizeof(char) * ((int)index - x + 1));
	if (!temp)
		return (NULL);
	while (x < (int)index)
	{
		temp[cnt] = s[x];
		cnt++;
		x++;
	}
	temp[cnt] = '\0';
	return (temp);
}

char		**ft_strsplit(char const *s, char c)
{
	char	**result;
	int		ind;
	int		cntr1;

	ind = 0;
	cntr1 = 0;
	if (!s)
		return (NULL);
	result = (char **)malloc(sizeof(char *) * (ft_cntdelimwords(s, c) + 1));
	if (!result)
		return (NULL);
	while (s[ind])
	{
		if (ft_isword((char *)s, ind, c))
		{
			result[cntr1] = get_prevword((char *)s, ind, c);
			cntr1++;
		}
		ind++;
	}
	if (ft_isword((char *)s, ind, c))
		result[cntr1++] = get_prevword((char *)s, ind, c);
	result[cntr1] = 0;
	return (result);
}
*/

char	*ft_strstr(const char *haystack, const char *needle)
{
	char	*start;
	char	*temp;

	start = "0";
	temp = "0";
	if (*needle == '\0')
		return ((char *)haystack);
	while (*haystack)
	{
		start = (char *)haystack;
		temp = (char *)needle;
		while (*haystack && *temp && *haystack == *temp)
		{
			haystack++;
			temp++;
		}
		if (!*temp)
			return (start);
		haystack = start + 1;
	}
	return (NULL);
}
/*
char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*str;
	size_t	i;

	i = 0;
	if (s == NULL)
		return (NULL);
	if (start > ft_strlen(s))
		return (NULL);
	str = (char *)malloc(sizeof(char) * (len + 1));
	if (!str)
		return (NULL);
	while (i < len && s[start] != '\0')
	{
		str[i] = s[start];
		start++;
		i++;
	}
	str[i] = '\0';
	return (str);
}

char	*ft_strtrim(char const *s)
{
	size_t	min;
	size_t	max;
	size_t	len;

	if (!s)
		return (NULL);
	min = 0;
	while (s[min] != '\0'
			&& (s[min] == ' ' || s[min] == '\n' || s[min] == '\t'))
		min++;
	max = ft_strlen(s);
	while (min < max
			&& (s[max - 1] == ' ' || s[max - 1] == '\n' || s[max - 1] == '\t'))
		max--;
	if (min == max)
		return (ft_strnew(1));
	len = max - min;
	return (ft_strsub(s, min, len));
}
*/

int	ft_tolower(int c)
{
	if (c >= 65 && c <= 90)
		return (c + 32);
	return (c);
}

int	ft_toupper(int c)
{
	if (c >= 97 && c <= 122)
		return (c - 32);
	return (c);
}

void	ft_swap(int *a, int *b)
{
	int	temp;

	temp = *a;
	*a = *b;
	*b = temp;
}

void itoa_hex(char *res, int num) {
	char *buf = res;
	unsigned long tmp = num;
	int div = 16;
	int cnt = 0;
	do {
		int reste = tmp % div;
		*buf++ = (reste < 10) ? reste + '0' : reste + 'a' - 10;
		cnt++;
	} while (tmp /= div);
	*buf = 0;

	char *p1, *p2;
	p1 = res;
	p2 = buf - 1;
	while (p1 < p2) {
		char t = *p1;
		*p1 = *p2;
		*p2 = t;
		p1++;
		p2--;
	}
}


