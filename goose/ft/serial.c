#include "ft.h"

static void serial_configure_baud_rate(unsigned short port, unsigned short rate) {
	outb(SERIAL_LINE_COMMAND_PORT(port),
		SERIAL_LINE_ENABLE_DLAB);
	outb(SERIAL_DATA_PORT(port),
		(rate >> 8) & 0x00FF);
	outb(SERIAL_DATA_PORT(port),
		rate & 0x00FF);
}

static void serial_configure_line(unsigned short port) {
	/* Bit:     | 7 | 6 | 5 4 3 | 2 | 1 0 |
	 * Content: | d | b | prty | s | dl |
	 * Value:   | 0 | 0 | 0 0 0 | 0 | 1 1 | = 0x03
	 */
	outb(SERIAL_LINE_COMMAND_PORT(port), 0x03);
}

static void serial_configure_fifo_buffer(unsigned short port) {
	outb(SERIAL_FIFO_COMMAND_PORT(port), 0xC7);
}


static void serial_configure_modem(unsigned short port) {
	outb(SERIAL_MODEM_COMMAND_PORT(port), 0x03);
}

static int serial_is_transmit_fifo_empty(unsigned int port) {
	/* 0x20 = 0010 0000 */
        return inb(SERIAL_LINE_STATUS_PORT(port)) & 0x20;
}

static void serial_write_byte(unsigned short port, char byte) {
	outb(port, byte);
}

static void serial_configure(unsigned short port, unsigned short baud_rate) {
	serial_configure_baud_rate(port, baud_rate);
	serial_configure_line(port);
	serial_configure_fifo_buffer(port);
	serial_configure_modem(port);
}
enum BaudRate rate_from_port(unsigned short port) {
	// TODO: get baud from sys.serial_ports table
	(void)port;
	return Baud_115200;
}

void serial_write(unsigned short port, const char *str, unsigned int size) {
	serial_configure(port, rate_from_port(port));
	for (unsigned int ind = 0; ind < size; ind++) {
		if (serial_is_transmit_fifo_empty(port)) {
			serial_write_byte(port, str[ind]);
		}
	}
}



