#include "goose.h"
#include "ft.h"

void kprint(const char *str) {
	// select display and call either serial_write or fb_write
	if (sys.out == console_serial_com1) {
		serial_write(SERIAL_COM1_BASE, str, ft_strlen(str));
	}
	else if (sys.out == console_vga) {
		fb_write_str(str, COLOR_WHITE, COLOR_BLACK);
	}
	else {
		serial_write(SERIAL_COM1_BASE, str, ft_strlen(str));
		fb_write_str(str, COLOR_WHITE, COLOR_BLACK);
	}
}

void kprint_hex(uint32_t addr) {
	char buf[10] = "0xCAFEBABE";

	char base[16] = "0123456789abcdef";
	int len = 10;
	int count = 0;
	while (addr != 0) {
		buf[--len] = base[addr % 16];
		addr = addr / 16;
		count++;
	}
	while (count < 9) {
		buf[--len] = '0';
		count++;
	}
	buf[0] = '0';
	buf[1] = 'x';
	kprint(buf);
}

void kprint_stack() {
	kprint("Printing goose stack: ");
	int ebp;
	int esp;
	asm volatile("mov %%ebp, %0" : "=r"(ebp) ::);
	asm volatile("mov %%esp, %0" : "=r"(esp) ::);
	kprint("esp: ");
	kprint_hex(esp);
	kprint(" ebp: ");
	kprint_hex(ebp);
	kprint("\n\n");
	uint32_t cur = esp;
	char *tmp = 0;
	char res[20];
	while (cur < (uint32_t)ebp) {
		kprint_hex(cur);
		kprint(": ");
		tmp = (char *)cur;
		int to_print = 0;
		if ((ebp - cur) >= 16)
			to_print = 16;
		else
			to_print = (int)(ebp - cur);
		for (int i = 0; i < to_print; i++) {
			if (tmp[i] == 0) {
				res[0] = res[1] = '0';
				res[2] = 0;
				kprint(res);
			}
			else {
				if (tmp[i] < 16) {
					res[0] = '0';
					itoa_hex(res + 1, tmp[i]);	
				}
				else {
					itoa_hex(res, tmp[i]);		
				}
				res[2] = 0;
				kprint(res);
			}
			kprint(" ");
			ft_bzero(res, 20);
		}
		if (to_print < 16) {
			for (int i = 0; i < 16 - to_print; i++) {
				kprint("   ");
			}
		}
		for (int j = 0; j < to_print; j++) {
			if (ft_isprint(tmp[j])) {
				char c = tmp[j];
				kprint(&c);
			}
			else
				kprint(".");
		}
		cur += 16;
		kprint("\n");
	}
}