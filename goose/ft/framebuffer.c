#include "goose.h"
#include "ft.h"

fb framebuffer = (fb) {
	.p_vga = (char *)VGA_PTR,
	.pos_x = 0,
	.pos_y = 0,
};

void fb_set_current(char *cur, int id, unsigned char value) {
	*(cur + id) = value;
}

void fb_clear() {	
	for (int i = 0; i < COLUMNS * LINES * 2; i++) {
		char *video = (char *)framebuffer.p_vga;
		fb_set_current(video, i, 0);
	}
	framebuffer.pos_x = 0;
	framebuffer.pos_y = 0;
}


void fb_write(unsigned int i, char c, unsigned char fg_color, unsigned char bg_color) {
	/* 
	 * vga layout in ram is :
	 *	Bit:     | 15 14 13 12 11 10 9 8 | 7 6 5 4 | 3 2 1 0 |
	 * 	Content: | ASCII                 | BG      | FG      |
	 * source : little os dev
	 */
	framebuffer.p_vga[i] = c;
	framebuffer.p_vga[i + 1] = ((bg_color & 0x0F) << 4) | (fg_color & 0x0F);
}

static void inc_cur () {
	framebuffer.pos_x += 1;
	if (framebuffer.pos_x >= COLUMNS) {
		framebuffer.pos_x = 0;
		framebuffer.pos_y += 1;
		if (framebuffer.pos_y >= LINES) {
			// BANZAAAAI !! I hope you didn't log anything important
			framebuffer.pos_y = 0;
		}
	}
}

static void inc_cur_line() {
	framebuffer.pos_x = 0;
	framebuffer.pos_y += 1;
	if (framebuffer.pos_y >= LINES) {
		framebuffer.pos_y = 0;
	}
}

void fb_write_str(const char *str, unsigned char fg_color, unsigned char bg_color) {
	int len = ft_strlen(str);
	if ((framebuffer.pos_x == 0) && (framebuffer.pos_y == 0)) {
		fb_clear();
	}

	for (int i = 0; i < len; i += 1) {
		int index = (framebuffer.pos_x + (framebuffer.pos_y * COLUMNS)) * 2;
		if (ft_isprint(str[i])) {
			fb_write(index, str[i], fg_color, bg_color);
			inc_cur();
		}
		else if (str[i] == ' ') {
			fb_write(index, str[i], fg_color, bg_color);
			inc_cur();
		}
		else if (str[i] == '\n') {
			inc_cur_line();
		}
	}

}

