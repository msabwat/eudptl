#ifndef GOOSE_H
#define GOOSE_H
#define STACK_SIZE 666
#define NULL (void *)0

typedef unsigned char           uint8_t;
typedef char			int8_t;
typedef unsigned short          uint16_t;
typedef short			int16_t;
typedef unsigned int            uint32_t;
typedef int			size_t;
typedef int			int32_t;
typedef unsigned long long      uint64_t;
typedef long long		int64_t;

typedef struct gdt_entry_struct
{
   uint16_t	limit_low;
   uint16_t	base_low;
   uint8_t	base_middle;
   uint8_t	access;
   uint8_t	granularity;
   uint8_t	base_high;
} __attribute__((packed)) gdt_entry_t;

typedef struct gdt_ptr_struct
{
   uint16_t	limit;
   uint32_t	base;
} __attribute__((packed)) gdt_ptr_t;

enum console { console_all, console_vga, console_serial_com1 };

typedef struct sys {
	gdt_ptr_t 	*p_gdt;
	gdt_entry_t	gdt[7];
	/* this option will control the behavior of kprint */
	enum console	out;
} __attribute__((packed)) sys_t;

extern sys_t sys;
extern void _asm_gdt_flush(uint32_t p_gdt);
void init_descriptor_tables(sys_t *sys);

/* GDT macros from osdev wiki */
#define SEG_DESCTYPE(x)  ((x) << 0x04) // Descriptor type (0 for system, 1 for code/data)
#define SEG_PRES(x)      ((x) << 0x07) // Present
#define SEG_SAVL(x)      ((x) << 0x0C) // Available for system use
#define SEG_LONG(x)      ((x) << 0x0D) // Long mode
#define SEG_SIZE(x)      ((x) << 0x0E) // Size (0 for 16-bit, 1 for 32)
#define SEG_GRAN(x)      ((x) << 0x0F) // Granularity (0 for 1B - 1MB, 1 for 4KB - 4GB)
#define SEG_PRIV(x)     (((x) &  0x03) << 0x05)   // Set privilege level (0 - 3)
 
#define SEG_DATA_RD        0x00 // Read-Only
#define SEG_DATA_RDA       0x01 // Read-Only, accessed
#define SEG_DATA_RDWR      0x02 // Read/Write
#define SEG_DATA_RDWRA     0x03 // Read/Write, accessed
#define SEG_DATA_RDEXPD    0x04 // Read-Only, expand-down
#define SEG_DATA_RDEXPDA   0x05 // Read-Only, expand-down, accessed
#define SEG_DATA_RDWREXPD  0x06 // Read/Write, expand-down
#define SEG_DATA_RDWREXPDA 0x07 // Read/Write, expand-down, accessed
#define SEG_CODE_EX        0x08 // Execute-Only
#define SEG_CODE_EXA       0x09 // Execute-Only, accessed
#define SEG_CODE_EXRD      0x0A // Execute/Read
#define SEG_CODE_EXRDA     0x0B // Execute/Read, accessed
#define SEG_CODE_EXC       0x0C // Execute-Only, conforming
#define SEG_CODE_EXCA      0x0D // Execute-Only, conforming, accessed
#define SEG_CODE_EXRDC     0x0E // Execute/Read, conforming
#define SEG_CODE_EXRDCA    0x0F // Execute/Read, conforming, accessed
 
#define GDT_CODE_RING0	SEG_DESCTYPE(1) | SEG_PRES(1) | SEG_SAVL(0) | \
			SEG_LONG(0)     | SEG_SIZE(1) | SEG_GRAN(1) | \
			SEG_PRIV(0)     | SEG_CODE_EXRD
 
#define GDT_DATA_RING0	SEG_DESCTYPE(1) | SEG_PRES(1) | SEG_SAVL(0) | \
			SEG_LONG(0)     | SEG_SIZE(1) | SEG_GRAN(1) | \
			SEG_PRIV(0)     | SEG_DATA_RDWR

#define GDT_STACK_RING0	SEG_DESCTYPE(1) | SEG_PRES(1) | SEG_SAVL(0) | \
			SEG_LONG(0)     | SEG_SIZE(1) | SEG_GRAN(1) | \
			SEG_PRIV(0)     | SEG_DATA_RDWREXPD
 
#define GDT_CODE_URING	SEG_DESCTYPE(1) | SEG_PRES(1) | SEG_SAVL(0) | \
			SEG_LONG(0)     | SEG_SIZE(1) | SEG_GRAN(1) | \
			SEG_PRIV(3)     | SEG_CODE_EXRD
 
#define GDT_DATA_URING	SEG_DESCTYPE(1) | SEG_PRES(1) | SEG_SAVL(0) | \
			SEG_LONG(0)     | SEG_SIZE(1) | SEG_GRAN(1) | \
			SEG_PRIV(3)     | SEG_DATA_RDWR

#define GDT_STACK_URING	SEG_DESCTYPE(1) | SEG_PRES(1) | SEG_SAVL(0) | \
			SEG_LONG(0)     | SEG_SIZE(1) | SEG_GRAN(1) | \
			SEG_PRIV(3)     | SEG_DATA_RDWREXPD

#endif
